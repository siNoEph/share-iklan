<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/frontend/images/favicon.png">
    <title>Share Iklan</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/frontend/css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/frontend/type/icons.css" rel="stylesheet">

    <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.min.js"></script>
</head>

<body>
    <div id="preloader">
        <div class="textload">Loading...</div>
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <main class="body-wrapper">
        <div id="top-header">
            <div class="row">
                <div class="col-md-6 left">
                    <div class="item-left"><?php echo $this->lang->line('welcome'); ?></div>
                    <div class="item-left"><?php echo $contact->email; ?></div>
                    <div class="item-left"><?php echo $contact->phone; ?></div>
                </div>
                <!-- .left -->
                <div class="col-md-6 right">
                    <select onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;" class="language">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                        <option value="indonesia" <?php if($this->session->userdata('site_lang') == 'indonesia') echo 'selected="selected"'; ?>>Indonesia</option>
                    </select>
                    <ul class="social">
                        <li><a href="<?php echo $contact->link_fb; ?>" target="_blank"><i class="icon-s-facebook"></i></a></li>
                        <li><a href="<?php echo $contact->link_ig; ?>" target="_blank"><i class="icon-s-instagram"></i></a></li>
                        <li><a href="<?php echo $contact->link_tw; ?>" target="_blank"><i class="icon-s-twitter"></i></a></li>
                    </ul>
                </div>
                <!-- .right -->
            </div>
        </div>
        </div>
        <div class="navbar solid" data-spy="affix" data-offset-top="44">
            <div class="navbar-header">
                <div class="basic-wrapper">
                    <div class="navbar-brand">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/logo.png" class="logo-light" alt="Share Iklan" /></a>
                    </div>
                    <a class="btn responsive-menu" data-toggle="collapse" data-target=".navbar-collapse"><i></i></a> </div>
            </div>
            <!-- /.navbar-header -->
            <nav class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#how-it-works"><?php echo $this->lang->line('how_it_works'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#about-us"><?php echo $this->lang->line('about_us'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#best-tvc"><?php echo $this->lang->line('best_tvc'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#testimony"><?php echo $this->lang->line('testimony'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#update"><?php echo $this->lang->line('update'); ?></a></a></li>
                    <li><a href="<?php echo base_url(); ?>#partner"><?php echo $this->lang->line('partner'); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>#contact"><?php echo $this->lang->line('contact'); ?></a></li>
                    <!-- <li class="hidden-md hidden-lg"><a href="#">Login</a></li> -->
                </ul>
                <!-- /.navbar-nav -->
            </nav>
            <!-- /.navbar-collapse -->
            <div class="social-wrapper">
                <ul class="social naked navbar-right">
                    <li><a href="#"><?php echo $this->lang->line('login'); ?></a></li>
                </ul>
            </div>
        </div>
        <!-- /.navbar -->