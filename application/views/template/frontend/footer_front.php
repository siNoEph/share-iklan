        <section id="contact">
            <div class="red-wrapper">
                <div class="container inner3">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('contact'); ?></h3>
                    <div class="divide10"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="map"></div>
                            <script>
                            function initMap() {
                                var mapDiv = document.getElementById('map');
                                var latLng = {
                                    lat: <?php echo $contact->latitude; ?>,
                                    lng: <?php echo $contact->longitude; ?>
                                };
                                var map = new google.maps.Map(mapDiv, {
                                    center: latLng,
                                    zoom: 17,
                                    zoomControl: true,
                                    zoomControlOptions: {
                                        style: google.maps.ZoomControlStyle.DEFAULT,
                                    },
                                    disableDoubleClickZoom: false,
                                    mapTypeControl: false,
                                    mapTypeControlOptions: {
                                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                                    },
                                    scaleControl: true,
                                    scrollwheel: false,
                                    streetViewControl: true,
                                    draggable: true,
                                    overviewMapControl: false,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                });
                                var contentString = '<div id="content"><h2 id="firstHeading" class="firstHeading"><?php echo $contact->name; ?></h2><div id="bodyContent"><p><i class="icon-location"></i> <?php echo $contact->address; ?><br /><i class="icon-phone"></i> <?php echo $contact->phone; ?> &nbsp <i class="icon-mail"></i> <?php echo $contact->email; ?></p></div></div>';

                                var infowindow = new google.maps.InfoWindow({
                                    content: contentString
                                });
                                var marker = new google.maps.Marker({
                                    position: latLng,
                                    map: map,
                                    title: '<?php echo $contact->name; ?>'
                                });
                                marker.addListener('click', function() {
                                    infowindow.open(map, marker);
                                });
                            }
                            </script>
                            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiUu-1jHySHuJkcPy_pucOoqy7Dcs7ABU&callback=initMap"></script>
                        </div>
                        <div class="col-sm-6">
                            <h3 class="text-center"><?php echo $contact->name; ?></h3>
                            <ul class="contact-info text-center">
                                <li><i class="icon-location"></i><?php echo $contact->address; ?></li>
                                <li><i class="icon-phone"></i><a href="tel:<?php echo $contact->phone; ?>"><?php echo $contact->phone; ?></a></li>
                                <li><i class="icon-mail"></i><a href="mailto:<?php echo $contact->email; ?>"><?php echo $contact->email; ?></a> </li>
                            </ul>
                            <div class="divide30"></div>
                            <div class="form-container">
                                <form action="#" method="POST" class="vanilla vanilla-form form-contact">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="text" name="name" id="name-contact" placeholder="<?php echo $this->lang->line('your_name'); ?>" required>
                                                </label>
                                            </div>
                                            <!--/.form-field -->
                                        </div>
                                        <!--/column -->
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="email" name="email" id="email-contact" placeholder="<?php echo $this->lang->line('your_email'); ?>" required>
                                                </label>
                                            </div>
                                            <!--/.form-field -->
                                        </div>
                                        <!--/column -->
                                    </div>
                                    <!--/.row -->
                                    <textarea name="message" id="comment-contact" placeholder="<?php echo $this->lang->line('your_comment'); ?>" required></textarea>
                                    <input type="submit" class="btn btn-red col-sm-12 submit-contact" value="Send">
                                    <footer class="notification-box"></footer>
                                </form>
                                <!--/.vanilla-form -->
                            </div>
                            <!--/.form-container -->
                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.light-wrapper -->
        </section>
        <!--/#contact -->
        <footer class="footer inverse-wrapper text-center">
            <div class="container inner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget">
                            <ul class="social">
                                <li><a href="<?php echo $contact->link_fb; ?>" target="_blank"><i class="icon-s-facebook"></i></a></li>
                                <li><a href="<?php echo $contact->link_ig; ?>" target="_blank"><i class="icon-s-instagram"></i></a></li>
                                <li><a href="<?php echo $contact->link_tw; ?>" target="_blank"><i class="icon-s-twitter"></i></a></li>
                            </ul>
                            <!-- .social -->
                        </div>
                        <div class="widget">
                            <ul class="tag-list">
                                <li><a href="<?php echo base_url(); ?>#home" class="btn"><?php echo $this->lang->line('home'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#how-it-works" class="btn"><?php echo $this->lang->line('how_it_works'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#about-us" class="btn"><?php echo $this->lang->line('about_us'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#best-tvc" class="btn"><?php echo $this->lang->line('best_tvc'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#testimony" class="btn"><?php echo $this->lang->line('testimony'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#update" class="btn"><?php echo $this->lang->line('update'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#partner" class="btn"><?php echo $this->lang->line('partner'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>#contact" class="btn"><?php echo $this->lang->line('contact'); ?></a></li>
                            </ul>
                        </div>
                        <!-- /.widget -->
                    </div>
                    <!-- /column -->
                    <div class="col-md-12 tm20">
                        <p>Copyright © 2016 ShareIklan. All rights reserved.</p>
                    </div>
                    <!-- /column -->
                </div>
                <!-- /.row -->
            </div>
            <!-- .container -->
        </footer>
        <!-- /footer -->
    </main>
    <div class="modal fade" id="popupMessage" tabindex="-1" role="dialog" aria-labelledby="Message">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                </div>
                <div class="modal-body">
                    <p id="textMessage"></p>
                </div>
            </div>
        </div>
    </div>

    <!--/.body-wrapper -->
    <script src="<?php echo base_url(); ?>assets/frontend/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/plugins.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/classie.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/frontend/js/scripts.js"></script>
</body>

</html>