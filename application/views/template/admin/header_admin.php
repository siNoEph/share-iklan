<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $page_title; ?></title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/colors.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>assets/backend/css/custom.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/forms/selects/bootstrap_select.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/plugins/editors/summernote/summernote.min.js"></script>

	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/core/app.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/pages/datatables_basic.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/backend/js/pages/form_bootstrap_select.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="<?php echo base_url(); ?>admin"><img src="<?php echo base_url(); ?>assets/frontend/images/logo.png" alt=""></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<p class="navbar-text"><span class="label bg-success-400">Online</span></p>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url(); ?>assets/backend/images/placeholder.jpg" alt="">
						<span><?php echo $session_login->first_name." ".$session_login->last_name; ?></span>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<a href="#" class="media-left"><img src="<?php echo base_url(); ?>assets/backend/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $session_login->first_name." ".$session_login->last_name; ?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-envelop2 text-size-small"></i> &nbsp;<?php echo $session_login->email; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
								<li <?php echo ($active_menu == "home" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
								<li <?php echo ($active_menu == "slider" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/slider"><i class="icon-images2"></i> <span>Slider</span></a></li>
								<li <?php echo ($active_menu == "how_it_work" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/how_it_work"><i class="icon-hammer-wrench"></i> <span>How It Work</span></a></li>
								<li <?php echo ($active_menu == "about_us" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/about_us"><i class="icon-vcard"></i> <span>About Us</span></a></li>
								<li <?php echo ($active_menu == "faq" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/faq"><i class="icon-question3"></i> <span>FAQ</span></a></li>
								<li <?php echo ($active_menu == "term" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/term"><i class="icon-book3"></i> <span>Terms Conditions</span></a></li>
								<li <?php echo ($active_menu == "video_iklan" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/video_iklan"><i class="icon-video-camera"></i> <span>Advertisement</span></a></li>
								<li <?php echo ($active_menu == "testimony" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/testimony"><i class="icon-quotes-left"></i> <span>Testimony</span></a></li>
								<li <?php echo ($active_menu == "article" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/article"><i class="icon-newspaper"></i> <span>Article</span></a></li>
								<li <?php echo ($active_menu == "partner" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/partner"><i class="icon-people"></i> <span>Partner</span></a></li>
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>admin"><i class="icon-home2 position-left"></i> Home</a></li>
							<li class="active">Dashboard</li>
						</ul>

						<ul class="breadcrumb-elements">
							<?php /*
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>Settings <span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li <?php echo ($active_menu == "contact" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/contact"><i class="icon-magazine"></i> Contact Web</a></li>
								</ul>
							</li>
							*/ ?>
							<li <?php echo ($active_menu == "contact" ? "class='active'" : ""); ?>><a href="<?php echo base_url(); ?>admin/contact"><i class="icon-magazine"></i> Contact Web</a></li>
							<li><a href="<?php echo base_url(); ?>admin/auth/logout"><i class="icon-switch2 position-left"></i>Logout</a></li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">