<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <title>404 Page Not Found</title>
        <style type="text/css">
        body {
            background-color: #fff;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
        }
        
        .error-title {
            color: #fff;
            font-size: 200px;
            line-height: 1;
            margin-bottom: 20px;
            font-weight: 300;
            text-stroke: 1px transparent;
            display: block;
            text-shadow: 0 1px 0 #ccc, 0 2px 0 #c9c9c9, 0 3px 0 #bbb, 0 4px 0 #b9b9b9, 0 5px 0 #aaa, 0 6px 1px rgba(0, 0, 0, 0.1), 0 0 5px rgba(0, 0, 0, 0.1), 0 1px 3px rgba(0, 0, 0, 0.3), 0 3px 5px rgba(0, 0, 0, 0.2), 0 5px 10px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.2), 0 20px 20px rgba(0, 0, 0, 0.15);
        }
        
        .content-group {
            margin-bottom: 20px !important;
        }
        
        .text-semibold {
            font-weight: 500;
        }
        
        .text-center {
            text-align: center;
        }
        </style>
    </head>

    <body>
        <div class="container-fluid text-center">
            <h1 class="error-title">404</h1>
            <h3 class="text-semibold content-group">Oops, an error has occurred. Page not found!</h3>
        </div>
    </body>
    </html>
