<?php $this->view('template/frontend/header_front'); ?>
<div class="light-wrapper">
    <div class="container padd-from-nav">
        <div class="blog row">
            <div class="col-sm-8 blog-content">
                <div class="blog-posts classic-view">
                    <div class="post">
                        <div class="box text-center" style="padding: 30px;">
                            <h1 class="post-title"><?php echo $this->session->userdata('site_lang') == 'english' ? $detail_article->title : $detail_article->title_id; ?></h1>
                            <div class="meta"><span class="date"><i class="icon-calendar-1"></i> <?php echo date("d M Y", strtotime($detail_article->date)); ?></span></div>
                            <figure class="main"><img src="<?php echo base_url(); ?>assets/frontend/images/article/<?php echo $detail_article->image; ?>" alt="" /></figure>
                            <div class="post-content text-left">
                                <?php echo $this->session->userdata('site_lang') == 'english' ? $detail_article->description : $detail_article->description_id; ?>
                            </div>
                        </div>
                    </div>
                    <div class="divide30"></div>
                </div>
            </div>
            <!-- /.blog-content -->
            <aside class="col-sm-4 sidebar">
                <div class="sidebox widget">
                    <h3 class="widget-title"><?php echo $this->lang->line('search'); ?></h3>
                    <form class="searchform" method="get">
                        <input type="text" placeholder="<?php echo $this->lang->line('text_search'); ?>" style="background: #fafbfc;">
                        <button type="submit" class="btn btn-blue"><?php echo $this->lang->line('button_search'); ?></button>
                    </form>
                </div>
                <div class="sidebox widget">
                    <figure><img src="<?php echo base_url(); ?>assets/frontend/images/logo.png" alt="" style="width: auto;" /></figure>
                    <p>PT Shareiklan Indonesia established in August 2016, is a start-up targeted ad-blaster media.</p>
                    <ul class="social">
                        <li><a href="<?php echo $contact->link_fb; ?>" target="_blank"><i class="icon-s-facebook"></i></a></li>
                        <li><a href="<?php echo $contact->link_ig; ?>" target="_blank"><i class="icon-s-instagram"></i></a></li>
                        <li><a href="<?php echo $contact->link_tw; ?>" target="_blank"><i class="icon-s-twitter"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="sidebox widget">
                    <h3 class="widget-title"><?php echo $this->lang->line('other_post'); ?></h3>
                    <ul class="post-list">
                        <?php
                        if ($other_article != null) {
                            foreach ($other_article as $value) { ?>
                                <li>
                                    <div class="icon-overlay">
                                        <a href="<?php echo base_url()."article/detail/".$value->slug; ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/article/<?php echo $value->image; ?>" alt="" /> </a>
                                    </div>
                                    <div class="meta">
                                        <h5><a href="<?php echo base_url()."article/detail/".$value->slug; ?>"><?php echo $this->session->userdata('site_lang') == 'english' ? substr($value->title, 0, 40)."..." : substr($value->title_id, 0, 40)."..."; ?></a></h5>
                                        <div class="meta" style="margin-left: 0px;"><span class="date"><i class="icon-calendar-1"></i> <?php echo date("d M Y", strtotime($value->date)); ?></span></div>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </aside>
        </div>
    </div>
</div>
<?php $this->view('template/frontend/footer_front'); ?>