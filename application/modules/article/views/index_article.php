<?php $this->view('template/frontend/header_front'); ?>
<div class="light-wrapper">
    <div class="container padd-from-nav">
        <div class="blog grid-view col3 bp20">
            <div class="blog-posts text-boxes">
                <div class="isotope row">
                    <?php
                    if ($all_article != null) {
                        foreach ($all_article as $value) { ?>
                            <div class="col-sm-6 col-md-4 grid-view-post">
                                <div class="post">
                                    <figure class="main">
                                        <a href="<?php echo base_url()."article/detail/".$value->slug; ?>"><img src="<?php echo base_url(); ?>assets/frontend/images/article/<?php echo $value->image; ?>" alt="" /></a>
                                    </figure>
                                    <div class="box text-center">
                                        <h4 class="post-title"><a href="<?php echo base_url()."article/detail/".$value->slug; ?>"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->title : $value->title_id; ?></a></h4>
                                        <div class="meta"><span class="date"><i class="icon-calendar-1"></i> <?php echo date("d M Y", strtotime($value->date)); ?></span></div>
                                        <?php echo $this->session->userdata('site_lang') == 'english' ? substr($value->description, 0, 140)."..." : substr($value->description_id, 0, 140)."..."; ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="pagination">
                <ul>
                    <?php echo $links; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
<?php $this->view('template/frontend/footer_front'); ?>