<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('home/Home_model');
		$this->load->model('Article_model');
		$this->load->library('pagination');
		$this->lang->load('bahasa','english');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$total_row = $this->Article_model->record_count();

		$config = array();
		$config["base_url"] = base_url() . "article/page";
		$config["total_rows"] = $total_row;
		$config["per_page"] = 6;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = $total_row;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a style="background: #fafbfc;"><span>';
		$config['cur_tag_close'] = '</span></a></li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';

		$this->pagination->initialize($config);

		$data["all_article"] = $this->Article_model->get_all_article($config["per_page"], 1);
		$data['contact'] = $this->Home_model->get_contact();

		$data["links"] = $this->pagination->create_links();

		// View data according to array.
		$this->load->view('index_article', $data);		
	}

	public function page()
	{
		$total_row = $this->Article_model->record_count();

		$config = array();
		$config["base_url"] = base_url() . "article/page";
		$config["total_rows"] = $total_row;
		$config["per_page"] = 6;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = $total_row;
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a style="background: #fafbfc;"><span>';
		$config['cur_tag_close'] = '</span></a></li>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';

		$this->pagination->initialize($config);
		if($this->uri->segment(3)){
			$page = ($this->uri->segment(3));
		}
		else{
			$page = 1;
		}

		$data["all_article"] = $this->Article_model->get_all_article($config["per_page"], $page);
		$data['contact'] = $this->Home_model->get_contact();

		$data["links"] = $this->pagination->create_links();

		// View data according to array.
		$this->load->view('index_article', $data);		
	}

	public function detail()
	{
		if($this->uri->segment(3)){
			$slug = ($this->uri->segment(3));

			if ($this->Article_model->get_detail_article($slug)) {
				$data["detail_article"] = $this->Article_model->get_detail_article($slug);
				$data["other_article"] = $this->Article_model->get_other_article($slug);
				$data['contact'] = $this->Home_model->get_contact();

				$this->load->view('detail_article', $data);
			} else {
				show_404();
			}
		}	
	}
}
