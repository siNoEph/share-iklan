<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_all_article($limit, $page) {
                $this->db->where("status", 1);
                $this->db->order_by("date", "desc"); 

                $offset = ($page-1)*$limit;
                $query = $this->db->get("article", $limit, $offset);

                if ($query->num_rows() > 0) {
                        foreach ($query->result() as $row) {
                                $data[] = $row;
                        }
                        return $data;
                }
                return false;
        }

        public function record_count() {
                return $this->db->count_all("article");
        }

        public function get_detail_article($slug)
        {
                $this->db->where("slug", $slug);
                $query = $this->db->get('article');
                
                return $query->row();
        }

        public function get_other_article($slug)
        {
                $this->db->where("status", 1);
                $this->db->where('slug !=', $slug);
                $this->db->order_by("date", "desc"); 
                $query = $this->db->get('article', 5);
                
                return $query->result();
        }
}