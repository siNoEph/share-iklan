<?php $this->view('template/frontend/header_front'); ?>
<div class="light-wrapper">
    <div class="container padd-from-nav">
        <div class="blog row">
            <div class="col-sm-8 blog-content">
                <h3 class="section-title text-uppercase">FAQ</h3>
                <div class="blog-posts classic-view">
                    <div class="post">
                        <div class="panel-group" id="accordion">
                            <?php if ($faq != null) {
                                $i = 1;
                                foreach ($faq as $value) { ?>                                    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" class="panel-toggle active" data-parent="#accordion" href="#collapse<?php echo $value->id_faq; ?>"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->tanya : $value->tanya_id; ?></a>
                                            </h4>
                                        </div>
                                        <div id="collapse<?php echo $value->id_faq; ?>" class="panel-collapse collapse <?php echo $i == 1 ? "in" : ""; ?>">
                                            <div class="panel-body"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->jawab : $value->jawab_id; ?></div>
                                        </div>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="divide30"></div>
                </div>
            </div>
            <!-- /.blog-content -->
            <?php $this->view('template/frontend/right_detail'); ?>
        </div>
    </div>
</div>
<?php $this->view('template/frontend/footer_front'); ?>