<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_faq()
        {
                $this->db->order_by("sort", "asc"); 
                $query = $this->db->get('faq');
                
                return $query->result();
        }
}