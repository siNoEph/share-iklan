<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('home/Home_model');
		$this->load->model('Faq_model');
		$this->load->helper('email');
		$this->load->library('email');
		$this->lang->load('bahasa','english');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$data['faq'] = $this->Faq_model->get_faq();
		$data['all_article'] = $this->Home_model->get_all_article();
		$data['contact'] = $this->Home_model->get_contact();

		$this->load->view('index_faq', $data);
	}
}
