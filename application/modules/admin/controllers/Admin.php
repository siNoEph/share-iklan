<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model');
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('../admin/auth/login', 'refresh');
		} else {
			$this->data["session_login"] = $this->ion_auth->user()->row();
		}
		$this->data["page_title"] = "Adminpanel Share Iklan";
	}

	public function index()
	{
		$this->data["active_menu"] = "home";
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_admin');
		$this->load->view('template/admin/footer_admin');
	}

	public function slider()
	{
		$this->data["active_menu"] = "slider";
		$this->data["all_slider"] = $this->Admin_model->get_all_slider();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_slider');
		$this->load->view('template/admin/footer_admin');
	}

	public function delete_slider() {
		if ($this->uri->segment(3) != NULL && $this->uri->segment(4) != NULL) {
			$id = $this->uri->segment(3);
			$fileImage = $this->uri->segment(4);
			$this->Admin_model->delete_slider($id);
			unlink('./assets/frontend/images/slider/'.$fileImage);

			if ($this->uri->segment(5) != NULL) {
				$fileVideo = $this->uri->segment(5);
				unlink('./assets/frontend/video/'.$fileVideo);
			}
		}
		redirect('/admin/slider');
	}

	public function post_new_slider() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgSlider-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/slider/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgSlider = $dataImage['upload_data']['file_name'];
                $videoSlider = "";

                if($_FILES['video']['tmp_name'] != NULL) {
		            $nameVideo = "videoSlider-".$dateNow->format('YmdHis');

		            $config['upload_path'] = './assets/frontend/video/';
		            $config['allowed_types'] = 'mpeg|mkv|flv|mp4';
		            $config['file_name'] = $nameVideo;
		            $config['overwrite'] = false;

		            $this->load->library('upload', $config);

		            if (!$this->upload->do_upload('video')) {
						$this->response['status'] = "error";
						$this->response['message'] = "Sorry, any problem in saving data video";
						echo json_encode($this->response);
		                exit();
		            }
		            else {
		                $dataVideo = array('upload_data' => $this->upload->data());
		                $videoSlider = $dataVideo['upload_data']['file_name'];
		            }
				}

				$download_googleplay = $this->input->post('download_googleplay') != NULL ? $this->input->post('download_googleplay') : 0;
				$download_appstore = $this->input->post('download_appstore') != NULL ? $this->input->post('download_appstore') : 0;
				$status_link = $this->input->post('status_link') != NULL ? $this->input->post('status_link') : 0;

		        $data = array(
					"type" => $this->input->post('type'),
					"text_top" => $this->input->post('text_top'),
					"text_bottom" => $this->input->post('text_bottom'),
					"text_top_id" => $this->input->post('text_top_id'),
					"text_bottom_id" => $this->input->post('text_bottom_id'),
					"download_googleplay" => $download_googleplay,
					"download_appstore" => $download_appstore,
					"status_link" => $status_link,
					"link" => $this->input->post('link'),
					"type_link" => $this->input->post('type_link'),
					"image" => $imgSlider,
					"video" => $videoSlider,
					"sort" => $this->input->post('sort'),
					"status" => $this->input->post('status')
		        );

		        $this->Admin_model->input_new_slider($data);

				$this->response['status'] = "success";
				$this->response['message'] = "Successfully save new slider";
				echo json_encode($this->response);
            }
		}
	}

	public function post_update_slider() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgSlider-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/slider/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgSlider = $dataImage['upload_data']['file_name'];                

		        $old_image = $this->input->post('old_image');
		        unlink('./assets/frontend/images/slider/'.$old_image);
            }
		} else {
			$imgSlider = $this->input->post('old_image');
		}		

        if($_FILES['video']['tmp_name'] != NULL) {
            $nameVideo = "videoSlider-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/video/';
            $config['allowed_types'] = 'mpeg|mkv|flv|mp4';
            $config['file_name'] = $nameVideo;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('video')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data video";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataVideo = array('upload_data' => $this->upload->data());
                $videoSlider = $dataVideo['upload_data']['file_name'];
                
		        $old_video = $this->input->post('old_video');
		        if ($old_video != NULL) {
		        	unlink('./assets/frontend/video/'.$old_video);
		        }
            }
		} else {
			$videoSlider = $this->input->post('old_video');
		}		

        $data = array(
			"type" => $this->input->post('type'),
			"text_top" => $this->input->post('text_top'),
			"text_bottom" => $this->input->post('text_bottom'),
			"text_top_id" => $this->input->post('text_top_id'),
			"text_bottom_id" => $this->input->post('text_bottom_id'),
			"download_googleplay" => $this->input->post('download_googleplay'),
			"download_appstore" => $this->input->post('download_appstore'),
			"status_link" => $this->input->post('status_link'),
			"link" => $this->input->post('link'),
			"type_link" => $this->input->post('type_link'),
			"image" => $imgSlider,
			"video" => $videoSlider,
			"sort" => $this->input->post('sort'),
			"status" => $this->input->post('status')
        );

        $id_slider = $this->input->post('id_slider');
        $this->Admin_model->input_update_slider($id_slider, $data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully update slider";
		echo json_encode($this->response);
	}

	public function how_it_work()
	{
		$this->data["active_menu"] = "how_it_work";
		$this->data["how_it_work"] = $this->Admin_model->get_how_it_work();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_how_it_work');
		$this->load->view('template/admin/footer_admin');
	}

	public function post_update_hiw()
	{
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImg = "icon-hiw-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/how_it_work/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImg;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->session->set_flashdata('status', "error");
				$this->session->set_flashdata('message', "Sorry, any problem in saving data image");
                redirect('/admin/how_it_work');
                exit();
            }
            else {
                $dataImg = array('upload_data' => $this->upload->data());
                $image = $dataImg['upload_data']['file_name'];
                
		        $old_image = $this->input->post('old_image');
		        if ($old_image != NULL) {
		        	unlink('./assets/frontend/images/how_it_work/'.$old_image);
		        }
            }
		} else {
			$image = $this->input->post('old_image');
		}

		$data = array(
			"text" => $this->input->post("text"),
			"description" => $this->input->post("description"),
			"text_id" => $this->input->post("text_id"),
			"description_id" => $this->input->post("description_id"),
			"image" => $image
        );

		$id_how_it_work = $this->input->post("id_how_it_work");
        $this->Admin_model->input_update_hiw($id_how_it_work, $data);

		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Successfully update how it work");
		redirect('/admin/how_it_work');
	}

	public function testimony()
	{
		$this->data["active_menu"] = "testimony";
		$this->data["testimony"] = $this->Admin_model->get_testimony();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_testimony');
		$this->load->view('template/admin/footer_admin');
	}

	public function delete_testimony() {
		if ($this->uri->segment(3) != NULL && $this->uri->segment(4) != NULL) {
			$id = $this->uri->segment(3);
			$fileImage = $this->uri->segment(4);
			$this->Admin_model->delete_testimony($id);
			unlink('./assets/frontend/images/testimony/'.$fileImage);
		}
		redirect('/admin/testimony');
	}

	public function post_new_testimony() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgTesti-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/testimony/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgTestimony = $dataImage['upload_data']['file_name'];

		        $data = array(
					"name" => $this->input->post('name'),
					"testimony" => $this->input->post('testimony'),
					"testimony_id" => $this->input->post('testimony_id'),
					"image" => $imgTestimony,
					"status" => $this->input->post('status')
		        );

		        $this->Admin_model->input_new_testimony($data);

				$this->response['status'] = "success";
				$this->response['message'] = "Successfully save new testimony";
				echo json_encode($this->response);
            }
		}
	}

	public function post_update_testimony() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgTesti-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/testimony/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgTestimony = $dataImage['upload_data']['file_name'];                

		        $old_image = $this->input->post('old_image');
		        unlink('./assets/frontend/images/testimony/'.$old_image);
            }
		} else {
			$imgTestimony = $this->input->post('old_image');
		}	

        $data = array(
			"name" => $this->input->post('name'),
			"testimony" => $this->input->post('testimony'),
			"testimony_id" => $this->input->post('testimony_id'),
			"image" => $imgTestimony,
			"status" => $this->input->post('status')
        );

        $id_testimony = $this->input->post('id_testimony');
        $this->Admin_model->input_update_testimony($id_testimony, $data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully update testimony";
		echo json_encode($this->response);
	}

	public function article()
	{
		$this->data["active_menu"] = "article";
		$this->data["article"] = $this->Admin_model->get_article();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_article');
		$this->load->view('template/admin/footer_admin');
	}

	public function delete_article() {
		if ($this->uri->segment(3) != NULL && $this->uri->segment(4) != NULL) {
			$id = $this->uri->segment(3);
			$fileImage = $this->uri->segment(4);
			$this->Admin_model->delete_article($id);
			unlink('./assets/frontend/images/article/'.$fileImage);
		}
		redirect('/admin/article');
	}

	public function post_new_article() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgArt-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/article/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgArticle = $dataImage['upload_data']['file_name'];

		        $data = array(
					"title" => $this->input->post('title'),
					"slug" => url_title($this->input->post('title'), 'dash', TRUE),
					"description" => $this->input->post('description'),
					"title_id" => $this->input->post('title_id'),
					"description_id" => $this->input->post('description_id'),
					"image" => $imgArticle,
					"date" => $dateNow->format('Y-m-d H:i:s'),
					"status" => $this->input->post('status')
		        );

		        $this->Admin_model->input_new_article($data);

				$this->response['status'] = "success";
				$this->response['message'] = "Successfully save new article";
				echo json_encode($this->response);
            }
		}
	}

	public function post_update_article() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgArt-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/article/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgArticle = $dataImage['upload_data']['file_name'];                

		        $old_image = $this->input->post('old_image');
		        unlink('./assets/frontend/images/article/'.$old_image);
            }
		} else {
			$imgArticle = $this->input->post('old_image');
		}	

        $data = array(
			"title" => $this->input->post('title'),
			"slug" => url_title($this->input->post('title'), 'dash', TRUE),
			"description" => $this->input->post('description'),
			"title_id" => $this->input->post('title_id'),
			"description_id" => $this->input->post('description_id'),
			"image" => $imgArticle,
			"date" => $dateNow->format('Y-m-d H:i:s'),
			"status" => $this->input->post('status')
        );

        $id_article = $this->input->post('id_article');
        $this->Admin_model->input_update_article($id_article, $data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully update article";
		echo json_encode($this->response);
	}

	public function partner()
	{
		$this->data["active_menu"] = "partner";
		$this->data["partner"] = $this->Admin_model->get_partner();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_partner');
		$this->load->view('template/admin/footer_admin');
	}

	public function delete_partner() {
		if ($this->uri->segment(3) != NULL && $this->uri->segment(4) != NULL) {
			$id = $this->uri->segment(3);
			$fileImage = $this->uri->segment(4);
			$this->Admin_model->delete_partner($id);
			unlink('./assets/frontend/images/partner/'.$fileImage);
		}
		redirect('/admin/partner');
	}

	public function post_new_partner() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgPart-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/partner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgPartner = $dataImage['upload_data']['file_name'];

		        $data = array(
					"name" => $this->input->post('name'),
					"link" => $this->input->post('link'),
					"image" => $imgPartner,
					"status" => $this->input->post('status')
		        );

		        $this->Admin_model->input_new_partner($data);

				$this->response['status'] = "success";
				$this->response['message'] = "Successfully save new partner";
				echo json_encode($this->response);
            }
		}
	}

	public function post_update_partner() {
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgPart-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/partner/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgPartner = $dataImage['upload_data']['file_name'];                

		        $old_image = $this->input->post('old_image');
		        unlink('./assets/frontend/images/partner/'.$old_image);
            }
		} else {
			$imgPartner = $this->input->post('old_image');
		}	

        $data = array(
			"name" => $this->input->post('name'),
			"link" => $this->input->post('link'),
			"image" => $imgPartner,
			"status" => $this->input->post('status')
        );

        $id_partner = $this->input->post('id_partner');
        $this->Admin_model->input_update_partner($id_partner, $data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully update partner";
		echo json_encode($this->response);
	}

	public function contact()
	{
		$this->data["active_menu"] = "contact";
		$this->data["contact"] = $this->Admin_model->get_contact();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_contact');
		$this->load->view('template/admin/footer_admin');
	}

	public function post_update_contact()
	{
		$data = array(
			"name" => $this->input->post("name"),
			"phone" => $this->input->post("phone"),
			"email" => $this->input->post("email"),
			"googleplay" => $this->input->post("googleplay"),
			"appstore" => $this->input->post("appstore"),
			"address" => $this->input->post("address"),
			"link_fb" => $this->input->post("link_fb"),
			"link_ig" => $this->input->post("link_ig"),
			"link_tw" => $this->input->post("link_tw"),
			"longitude" => $this->input->post("longitude"),
			"latitude" => $this->input->post("latitude")
        );

		$id_contact = $this->input->post("id_contact");
        $this->Admin_model->input_update_contact($id_contact, $data);

		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Successfully update contact");
		redirect('/admin/contact');
	}

	public function video_iklan()
	{
		$this->data["active_menu"] = "video_iklan";
		$this->data["video_iklan"] = $this->Admin_model->get_video_iklan();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_video_iklan');
		$this->load->view('template/admin/footer_admin');
	}

	public function post_update_video_iklan()
	{
		$dateNow = new DateTime();
		if($_FILES['video']['tmp_name'] != NULL) {
            $nameVideo = "videoIklan-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/video/';
            $config['allowed_types'] = 'mpeg|mkv|flv|mp4';
            $config['file_name'] = $nameVideo;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('video')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data video";
				echo json_encode($this->response);
                exit();
        	    }
            else {
                $dataVideo = array('upload_data' => $this->upload->data());
                $videoIklan = $dataVideo['upload_data']['file_name'];
                
		        $old_video = $this->input->post('old_video');
		        if ($old_video != NULL) {
		        	unlink('./assets/frontend/video/'.$old_video);
		        }
            }
		} else {
			$videoIklan = $this->input->post('old_video');
		}

		$data = array(
			"video" => $videoIklan,
			"title" => $this->input->post("title"),
			"description" => $this->input->post("description"),
			"title_id" => $this->input->post("title_id"),
			"description_id" => $this->input->post("description_id"),
			"status_link" => $this->input->post('status_link'),
			"link" => $this->input->post('link'),
			"type_link" => $this->input->post('type_link'),
			"status" => $this->input->post("status")
        );

		$id_video_iklan = $this->input->post("id_video_iklan");
        $this->Admin_model->input_update_video_iklan($id_video_iklan, $data);

		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Successfully update Video Iklan");
		redirect('/admin/video_iklan');
	}

	public function about_us()
	{
		$this->data["active_menu"] = "about_us";
		$this->data["about_us"] = $this->Admin_model->get_about_us();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_about_us');
		$this->load->view('template/admin/footer_admin');
	}

	public function post_update_about_us()
	{
		$dateNow = new DateTime();
		if($_FILES['image']['tmp_name'] != NULL) {
            $nameImage = "imgAbout-".$dateNow->format('YmdHis');

            $config['upload_path'] = './assets/frontend/images/about_us/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['file_name'] = $nameImage;
            $config['overwrite'] = false;

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('image')) {
				$this->response['status'] = "error";
				$this->response['message'] = "Sorry, any problem in saving data image.";
				echo json_encode($this->response);
                exit();
            }
            else {
                $dataImage = array('upload_data' => $this->upload->data());
                $imgAbout = $dataImage['upload_data']['file_name'];                

		        $old_image = $this->input->post('old_image');
		        unlink('./assets/frontend/images/about_us/'.$old_image);
            }
		} else {
			$imgAbout = $this->input->post('old_image');
		}

		$data = array(
			"image" => $imgAbout,
			"text" => $this->input->post("text"),
			"text_id" => $this->input->post("text_id")
        );

		$id_about_us = $this->input->post("id_about_us");
        $this->Admin_model->input_update_about_us($id_about_us, $data);

		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Successfully update About Us");
		redirect('/admin/about_us');
	}

	public function term()
	{
		$this->data["active_menu"] = "term";
		$this->data["term"] = $this->Admin_model->get_term();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_term');
		$this->load->view('template/admin/footer_admin');
	}

	public function post_update_term()
	{
		$data = array(
			"text" => $this->input->post("text_term"),
			"text_id" => $this->input->post("text_term_id")
        );

		$id_term = $this->input->post("id_term");
        $this->Admin_model->input_update_term($id_term, $data);

		$this->session->set_flashdata('status', "success");
		$this->session->set_flashdata('message', "Successfully Update Terms & Conditions");
		redirect('/admin/term');
	}

	public function faq()
	{
		$this->data["active_menu"] = "faq";
		$this->data["all_faq"] = $this->Admin_model->get_all_faq();
		$this->load->view('template/admin/header_admin', $this->data);
		$this->load->view('index_faq');
		$this->load->view('template/admin/footer_admin');
	}

	public function delete_faq() {
		if ($this->uri->segment(3) != NULL) {
			$id = $this->uri->segment(3);
			$this->Admin_model->delete_faq($id);
		}
		redirect('/admin/faq');
	}

	public function post_new_faq() {
		$data = array(
			"tanya" => $this->input->post('tanya'),
			"tanya_id" => $this->input->post('tanya_id'),
			"jawab" => $this->input->post('jawab'),
			"jawab_id" => $this->input->post('jawab_id'),
			"sort" => $this->input->post('sort')
        );

        $this->Admin_model->input_new_faq($data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully save new FAQ";
		echo json_encode($this->response);
	}

	public function post_update_faq() {
		$data = array(
			"tanya" => $this->input->post('tanya'),
			"tanya_id" => $this->input->post('tanya_id'),
			"jawab" => $this->input->post('jawab'),
			"jawab_id" => $this->input->post('jawab_id'),
			"sort" => $this->input->post('sort')
        );

        $id_faq = $this->input->post('id_faq');
        $this->Admin_model->input_update_faq($id_faq, $data);

		$this->response['status'] = "success";
		$this->response['message'] = "Successfully update FAQ";
		echo json_encode($this->response);
	}
}
