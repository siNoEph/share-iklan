<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_all_slider()
        {
                $query = $this->db->get('slider');
                
                return $query->result();
        }

        public function input_new_slider($data) {
                $this->db->insert('slider', $data);
        }

        public function input_update_slider($id, $data) {
                $this->db->where('id_slider', $id);
                $this->db->update('slider', $data);
        }

        public function delete_slider($id) {
                $this->db->where('id_slider', $id);
                $this->db->delete('slider');
        }

        public function get_how_it_work()
        {
                $query = $this->db->get('how_it_work');
                
                return $query->result();
        }

        public function input_update_hiw($id, $data) {
                $this->db->where('id_how_it_work', $id);
                $this->db->update('how_it_work', $data);
        }

        public function get_testimony()
        {
                $query = $this->db->get('testimony');
                
                return $query->result();
        }

        public function input_new_testimony($data) {
                $this->db->insert('testimony', $data);
        }

        public function input_update_testimony($id, $data) {
                $this->db->where('id_testimony', $id);
                $this->db->update('testimony', $data);
        }

        public function delete_testimony($id) {
                $this->db->where('id_testimony', $id);
                $this->db->delete('testimony');
        }

        public function get_article()
        {
                $this->db->order_by('date', 'desc');
                $query = $this->db->get('article');
                
                return $query->result();
        }

        public function input_new_article($data) {
                $this->db->insert('article', $data);
        }

        public function input_update_article($id, $data) {
                $this->db->where('id_article', $id);
                $this->db->update('article', $data);
        }

        public function delete_article($id) {
                $this->db->where('id_article', $id);
                $this->db->delete('article');
        }

        public function get_partner()
        {
                $query = $this->db->get('partner');
                
                return $query->result();
        }

        public function input_new_partner($data) {
                $this->db->insert('partner', $data);
        }

        public function input_update_partner($id, $data) {
                $this->db->where('id_partner', $id);
                $this->db->update('partner', $data);
        }

        public function delete_partner($id) {
                $this->db->where('id_partner', $id);
                $this->db->delete('partner');
        }

        public function get_contact()
        {
                $query = $this->db->get('contact');
                
                return $query->row();
        }

        public function input_update_contact($id, $data) {
                $this->db->where('id_contact', $id);
                $this->db->update('contact', $data);
        }

        public function get_video_iklan()
        {
                $query = $this->db->get('video_iklan');
                
                return $query->row();
        }

        public function input_update_video_iklan($id, $data) {
                $this->db->where('id_video_iklan', $id);
                $this->db->update('video_iklan', $data);
        }

        public function get_about_us()
        {
                $query = $this->db->get('about_us');
                
                return $query->row();
        }

        public function input_update_about_us($id, $data) {
                $this->db->where('id_about_us', $id);
                $this->db->update('about_us', $data);
        }

        public function get_all_faq()
        {
                $query = $this->db->get('faq');
                
                return $query->result();
        }

        public function input_new_faq($data) {
                $this->db->insert('faq', $data);
        }

        public function input_update_faq($id, $data) {
                $this->db->where('id_faq', $id);
                $this->db->update('faq', $data);
        }

        public function delete_faq($id) {
                $this->db->where('id_faq', $id);
                $this->db->delete('faq');
        }

        public function get_term()
        {
                $query = $this->db->get('term');
                
                return $query->row();
        }

        public function input_update_term($id, $data) {
                $this->db->where('id_term', $id);
                $this->db->update('term', $data);
        }
}