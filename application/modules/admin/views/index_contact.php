<div class="row">
	<?php
	$status = ($this->session->flashdata('status') == "success") ? "success" : "danger";
	$message = $this->session->flashdata('message');
	if (isset($message)) { ?>
		<div class="col-md-12">
		    <div class="alert alert-<?php echo $status; ?> alert-styled-left alert-arrow-left alert-bordered">
		        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><?php echo $message; ?></div>
		</div>
		<?php
	}

	if ($contact != null) { ?>
    	<form action="post_update_contact" method="POST" id="form-update-contact">
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Setting Contact</h5>
					</div>

					<div class="panel-body">
						<div class="form-group row">
							<div class="col-md-6">
								<style type="text/css">
									.controls {
								        margin-top: 10px;
								        border: 1px solid transparent;
								        border-radius: 2px 0 0 2px;
								        box-sizing: border-box;
								        -moz-box-sizing: border-box;
								        height: 32px;
								        outline: none;
								        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
								      }

								      #pac-input {
								        background-color: #fff;
								        font-family: Roboto;
								        font-size: 15px;
								        font-weight: 300;
								        margin-left: 12px;
								        padding: 0 10px 0 10px;
								        text-overflow: ellipsis;
								        width: 300px;
								      }

								      #pac-input:focus {
								        border-color: #4d90fe;
								      }
								</style>
								<input id="pac-input" class="controls" type="text" placeholder="Search Place" style="z-index: 1;position: absolute;">
								<div id="map_canvas" style="height: 325px;"></div>
	                            <script>
		                            function initMap() {
		                            	var myLatLng = {
											lat: <?php echo $contact->latitude; ?>, 
											lng: <?php echo $contact->longitude; ?>
										};

										var markers = [];
										var map = new google.maps.Map(document.getElementById('map_canvas'), {
											zoom: 15,
											center: myLatLng,
											mapTypeControl: false,
										    mapTypeId: google.maps.MapTypeId.ROADMAP
										});

										var marker = new google.maps.Marker({
											position: myLatLng,
											map: map,
											draggable: true
										});
										markers.push(marker);										

								        var input = document.getElementById('pac-input');
								        var searchBox = new google.maps.places.SearchBox(input);
								        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

										google.maps.event.addListener(searchBox, 'places_changed', function() {
										    var places = searchBox.getPlaces();
										    if (places.length == 0) {
										        return;
										    }
										    for (var i = 0, marker; marker = markers[i]; i++) {
										        marker.setMap(null);
										    }
										    markers = [];
										    var bounds = new google.maps.LatLngBounds();
										    for (var i = 0, place; place = places[i]; i++) {
										        var marker = new google.maps.Marker({
										            draggable: true,
										            map: map,
										            title: place.name,
										            position: place.geometry.location
										        });
										        // drag response
										        google.maps.event.addListener(marker, 'dragend', function(e) {
										            displayPosition(this.getPosition());
										        });
										        // click response
										        google.maps.event.addListener(marker, 'click', function(e) {
										            displayPosition(this.getPosition());
										        });
										        markers.push(marker);
										        bounds.extend(place.geometry.location);
										    }
										    map.fitBounds(bounds);
										});
										google.maps.event.addListener(map, 'bounds_changed', function() {
										    var bounds = map.getBounds();
										    searchBox.setBounds(bounds);
										});										

										google.maps.event.addListener(marker, 'dragend', function (evt) {
											document.getElementById('setLongitude').value = evt.latLng.lng().toFixed(3);
											document.getElementById('setLatitude').value = evt.latLng.lat().toFixed(3);
										});

										function displayPosition(pos) {
										    document.getElementById('setLatitude').value = pos.lat();
										    document.getElementById('setLongitude').value = pos.lng();
										}

									}
								</script>
                            	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiUu-1jHySHuJkcPy_pucOoqy7Dcs7ABU&libraries=places&callback=initMap"></script>
							</div>
							<div class="col-md-6">
								<label>Name</label>
								<input type="text" class="form-control" name="name" value="<?php echo $contact->name; ?>" required="required" style="margin-bottom: 20px;">

								<label>Address</label>
								<textarea rows="10" cols="5" name="address" class="form-control" required="required"><?php echo $contact->address; ?></textarea>
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-6">
								<label>Phone</label>
								<input type="text" class="form-control" name="phone" value="<?php echo $contact->phone; ?>" required="required">
							</div>

							<div class="col-md-6">
								<label>Email</label>
								<input type="email" class="form-control" name="email" value="<?php echo $contact->email; ?>" required="required">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-4">
								<label>Link Facebook</label>
								<input type="text" class="form-control" name="link_fb" value="<?php echo $contact->link_fb; ?>" required="required">
							</div>

							<div class="col-md-4">
								<label>Link Instagram</label>
								<input type="text" class="form-control" name="link_ig" value="<?php echo $contact->link_ig; ?>" required="required">
							</div>

							<div class="col-md-4">
								<label>Link Twitter</label>
								<input type="text" class="form-control" name="link_tw" value="<?php echo $contact->link_tw; ?>" required="required">
							</div>
						</div>

						<div class="form-group row">
							<div class="col-md-6">
								<label>Link Google Play</label>
								<input type="text" class="form-control" name="googleplay" value="<?php echo $contact->googleplay; ?>">
							</div>

							<div class="col-md-6">
								<label>Link App Store</label>
								<input type="text" class="form-control" name="appstore" value="<?php echo $contact->appstore; ?>">
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success btn-xs pull-right"><i class="icon-pencil7 position-left"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="id_contact" value="<?php echo $contact->id_contact; ?>">
			<input type="hidden" id="setLongitude" name="longitude" value="<?php echo $contact->longitude; ?>">
			<input type="hidden" id="setLatitude" name="latitude" value="<?php echo $contact->latitude; ?>">
		</form>
		<?php
	}
	?>
</div>