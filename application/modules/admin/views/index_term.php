<div class="row">
	<?php
	$status = ($this->session->flashdata('status') == "success") ? "success" : "danger";
	$message = $this->session->flashdata('message');
	if (isset($message)) { ?>
		<div class="col-md-12">
		    <div class="alert alert-<?php echo $status; ?> alert-styled-left alert-arrow-left alert-bordered">
		        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><?php echo $message; ?></div>
		</div>
		<?php
	}
	if ($term != null) { ?>
    	<form action="post_update_term" method="POST" id="form-update-term">
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Update Terms & Conditions</h5>
					</div>

					<div class="panel-body" id="update_term">
						<div class="form-group row">
							<div class="col-md-6">
								<label>English</label>
								<textarea rows="5" cols="5" name="text_term" id="text" style="display: none;" class="form-control"><?php echo $term->text; ?></textarea>
								<div class="summernote-term"><?php echo $term->text; ?></div>
							</div>
							<div class="col-md-6">
								<label>Indonesia</label>
								<textarea rows="5" cols="5" name="text_term_id" id="text_id" style="display: none;" class="form-control"><?php echo $term->text_id; ?></textarea>
								<div class="summernote-term-id"><?php echo $term->text_id; ?></div>
							</div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success btn-xs pull-right"><i class="icon-pencil7 position-left"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="id_term" value="<?php echo $term->id_term; ?>">
		</form>
		<script type="text/javascript">
			$('.summernote-term').summernote({
		        onChange: function() {
		            $("#update_term #text").val($('.summernote-term').code());
		        }
		    });
			$('.summernote-term-id').summernote({
		        onChange: function() {
		            $("#update_term #text_id").val($('.summernote-term-id').code());
		        }
		    });
		</script>
		<?php
	}
	?>
</div>