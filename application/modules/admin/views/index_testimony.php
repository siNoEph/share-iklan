<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Manage Testimony</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_testimony"><i class="icon-add position-left"></i> Add New</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <code>Testimony</code> with <strong>Image</strong>
    </div>
    <table class="table datatable-testimony">
        <thead>
            <tr>
                <th style="width: 150px;">Image</th>
                <th>Name</th>
                <th>Testimony</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($testimony != null) {
                foreach ($testimony as $value) { ?>
		            <tr>
		                <td>
		                	<div class="thumbnail" style="margin-bottom: 0px;">
								<img src="<?php echo base_url(); ?>assets/frontend/images/testimony/<?php echo $value->image; ?>" />
							</div>
						</td>
		                <td><?php echo $value->name; ?></td>
		                <td><?php echo $value->testimony; ?></td>
		                <td><?php
		                	if ($value->status == 1) {
		                		echo '<span class="label label-success">Active</span>';
		                	} else {
		                		echo '<span class="label label-default">Inactive</span>';
		                	} ?>
		                </td>
		                <td class="text-center">
		                    <ul class="icons-list">
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="icon-menu9"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-toggle="modal" data-target="#update_testimony_<?php echo $value->id_testimony; ?>"><i class="icon-pencil7"></i> Update</a></li>
		                                <li><a href="./delete_testimony/<?php echo $value->id_testimony; ?>/<?php echo $value->image; ?>" onClick="return confirm('Are you sure want to delete?')"><i class="icon-trash"></i> Delete</a></li>
		                            </ul>
		                        </li>
		                    </ul>
		                </td>
		            </tr>
                	<?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id="new_testimony" class="modal fade hidden-reload">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add New Testimony</h5>
			</div>

			<form action="#" method="POST" id="form-new-testimony" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-sm-12">
							<div id="message-submit"></div>
							<label>Name</label>
							<input type="text" class="form-control" name="name" id="name" required="required">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<label>Testimony (English)</label>
							<textarea rows="5" cols="5" name="testimony" class="form-control" required="required"></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<label>Testimony (Indonesia)</label>
							<textarea rows="5" cols="5" name="testimony_id" class="form-control" required="required"></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Image</label>
							<input type="file" name="image" id="image" class="form-control" required="required">
							<span class="help-block">Image required to testimony</span>
						</div>

						<div class="col-sm-6">
							<label>Status</label>
							<select class="bootstrap-select" data-width="100%" name="status" id="status">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary submit-new-testimony" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<?php
if ($testimony != null) {
    foreach ($testimony as $value) { ?>
		<div id="update_testimony_<?php echo $value->id_testimony; ?>" class="modal fade hidden-reload">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Update Testimony</h5>
					</div>

					<form action="#" method="POST" id="form-update-testimony-<?php echo $value->id_testimony; ?>" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="form-group row">
								<div class="col-sm-12">
									<div id="message-submit"></div>
									<label>Name</label>
									<input type="text" class="form-control" name="name" id="name" value="<?php echo $value->name; ?>" required="required">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-12">
									<label>Testimony (English)</label>
									<textarea rows="5" cols="5" name="testimony" class="form-control" required="required"><?php echo $value->testimony; ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-12">
									<label>Testimony (Indonesia)</label>
									<textarea rows="5" cols="5" name="testimony_id" class="form-control" required="required"><?php echo $value->testimony_id; ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Image</label>
									<input type="file" name="image" id="image" class="form-control">
									<span class="help-block">Image required to testimony</span>
								</div>

								<div class="col-sm-6">
									<label>Status</label>
									<select class="bootstrap-select" data-width="100%" name="status" id="status">
										<option value="1" <?php echo ($value->status == "1" ? "selected" : ""); ?>>Active</option>
										<option value="0" <?php echo ($value->status == "0" ? "selected" : ""); ?>>Inactive</option>
									</select>
								</div>
							</div>
						</div>
						<input type="hidden" name="id_testimony" value="<?php echo $value->id_testimony; ?>">
						<input type="hidden" name="old_image" value="<?php echo $value->image; ?>">

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary submit-update-testimony" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/* Submit form update testimony */
		    $("#form-update-testimony-<?php echo $value->id_testimony; ?>").submit(function(e) {
		        $.ajax({
		            type: "POST",
		            url: "post_update_testimony",
		            data: new FormData(this),
		            dataType: 'json',
		            processData: false,
		            contentType: false,
		            success: function(res) {
		                if (res.status == "success") {
		                    $("#form-update-testimony-<?php echo $value->id_testimony; ?> #message-submit").html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                    $("#form-update-testimony-<?php echo $value->id_testimony; ?>")[0].reset();
		                    setTimeout(function(){
		                    	$("#update_testimony_<?php echo $value->id_testimony; ?>").delay(3000).modal("hide");
		                    }, 3000);
		                } else {
		                    $("#form-update-testimony-<?php echo $value->id_testimony; ?> #message-submit").html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                }
		            }
		        });
		        e.preventDefault();
		    });
		</script>
    	<?php
    }
}
?>