<div class="row">
	<?php
	$status = ($this->session->flashdata('status') == "success") ? "success" : "danger";
	$message = $this->session->flashdata('message');
	if (isset($message)) { ?>
		<div class="col-md-12">
		    <div class="alert alert-<?php echo $status; ?> alert-styled-left alert-arrow-left alert-bordered">
		        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><?php echo $message; ?></div>
		</div>
		<?php
	}

	if ($video_iklan != null) { ?>
    	<form action="post_update_video_iklan" method="POST" id="form-update-video_iklan" enctype="multipart/form-data">
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Update Video Iklan</h5>
					</div>

					<div class="panel-body">
						<div class="form-group row">
							<div class="col-md-6">
								<div style="height: 325px;margin-bottom: 20px;">
									<video controls style="background-color: #000;width: 100%;height: 100%;">
                                        <source src="<?php echo base_url(); ?>assets/frontend/video/<?php echo $video_iklan->video; ?>" type="video/mp4" />
                                    </video>
								</div>
								<label class="checkbox-inline" style="margin-bottom: 20px;">
									<input type="checkbox" id="status_link" name="status_link" value="1" <?php echo ($video_iklan->status_link == "1" ? "checked='checked'" : ""); ?>> With Link
								</label>
								<div class="with-link" style="margin-bottom: 20px;">
									<label>Link</label>
									<input type="text" class="form-control" name="link" id="link" value="<?php echo $video_iklan->link; ?>">
								</div>
								<div class="with-link" style="margin-bottom: 20px;">
									<label>Type Link</label>
									<select class="bootstrap-select" data-width="100%" name="type_link" id="type_link">
										<option value="0" <?php echo ($video_iklan->type_link == "0" ? "selected" : ""); ?>>Self</option>
										<option value="1" <?php echo ($video_iklan->type_link == "1" ? "selected" : ""); ?>>Blank</option>
									</select>
								</div>
								<div style="margin-bottom: 20px;">
									<label>Video</label>
									<input type="file" name="video" class="form-control">
									<span class="help-block">*) Please upload file mpeg | mkv | flv | mp4</span>
								</div>

								<div style="margin-bottom: 20px;">
									<label>Status</label>
									<select class="bootstrap-select" data-width="100%" name="status">
										<option value="1" <?php echo ($video_iklan->status == "1" ? "selected" : ""); ?>>Active</option>
										<option value="0" <?php echo ($video_iklan->status == "0" ? "selected" : ""); ?>>Inactive</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<label>Title (English)</label>
								<input type="text" class="form-control" name="title" value="<?php echo $video_iklan->title; ?>" style="margin-bottom: 20px;">

								<label>Description (English)</label>
								<textarea rows="10" cols="5" name="description" class="form-control" style="margin-bottom: 20px;"><?php echo $video_iklan->description; ?></textarea>

								<label>Title (Indonesia)</label>
								<input type="text" class="form-control" name="title_id" value="<?php echo $video_iklan->title_id; ?>" style="margin-bottom: 20px;">

								<label>Description (Indonesia)</label>
								<textarea rows="10" cols="5" name="description_id" class="form-control" style="margin-bottom: 20px;"><?php echo $video_iklan->description_id; ?></textarea>

								<button type="submit" class="btn btn-success btn-xs pull-right"><i class="icon-pencil7 position-left"></i> Update</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="id_video_iklan" value="<?php echo $video_iklan->id_video_iklan; ?>">
			<input type="hidden" name="old_video" value="<?php echo $video_iklan->video; ?>">
		</form>		
		<script type="text/javascript">
			$(window).load(function() {
		    	if ($("#status_link").prop("checked") == false) {
		    		$(".with-link").hide();
		    	}
		    });
		    $("#status_link").click(function() {
		    	if ($(this).prop("checked") == true) {
		    		$(".with-link").show();
		    	} else if ($(this).prop("checked") == false) {
		    		$(".with-link").hide();
		    	}
		    });
		</script>
		<?php
	}
	?>
</div>