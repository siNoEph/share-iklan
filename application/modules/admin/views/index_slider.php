<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Manage Slider</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_slider"><i class="icon-add position-left"></i> Add New</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <code>Slider</code> support <strong>Image</strong> and <strong>Video</strong>
    </div>
    <table class="table datatable-slider">
        <thead>
            <tr>
                <th>Order</th>
                <th style="width: 150px;">Image or Video</th>
                <th>Text Top</th>
                <th>Text Bottom</th>
                <th>Type</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($all_slider != null) {
                foreach ($all_slider as $value) { ?>
		            <tr>
		                <td><?php echo $value->sort; ?></td>
		                <td>
		                	<div class="thumbnail" style="margin-bottom: 0px;">
								<img src="<?php echo base_url(); ?>assets/frontend/images/slider/<?php echo $value->image; ?>" />
							</div>
						</td>
		                <td><?php echo $value->text_top; ?></td>
		                <td><?php echo $value->text_bottom; ?></td>
		                <td><?php echo $value->type; ?></td>
		                <td><?php
		                	if ($value->status == 1) {
		                		echo '<span class="label label-success">Active</span>';
		                	} else {
		                		echo '<span class="label label-default">Inactive</span>';
		                	} ?>
		                </td>
		                <td class="text-center">
		                    <ul class="icons-list">
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="icon-menu9"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-toggle="modal" data-target="#update_slider_<?php echo $value->id_slider; ?>"><i class="icon-pencil7"></i> Update</a></li>
		                                <li><a href="./delete_slider/<?php echo $value->id_slider; ?>/<?php echo $value->image; ?>/<?php echo $value->video; ?>" onClick="return confirm('Are you sure want to delete?')"><i class="icon-trash"></i> Delete</a></li>
		                            </ul>
		                        </li>
		                    </ul>
		                </td>
		            </tr>
                	<?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id="new_slider" class="modal fade hidden-reload">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add New Slider</h5>
			</div>

			<form action="#" method="POST" id="form-new-slider" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-sm-12"><div id="message-submit"></div></div>
						<div class="col-sm-6">
							<label>Text Top (English)</label>
							<input type="text" class="form-control" name="text_top" id="text_top">
						</div>
						<div class="col-sm-6">
							<label>Text Top (Indonesia)</label>
							<input type="text" class="form-control" name="text_top_id" id="text_top_id">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Text Bottom (English)</label>
							<input type="text" class="form-control" name="text_bottom" id="text_bottom">
						</div>
						<div class="col-sm-6">
							<label>Text Bottom (Indonesia)</label>
							<input type="text" class="form-control" name="text_bottom_id" id="text_bottom_id">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-4">
							<label>Type</label>
							<select class="bootstrap-select" data-width="100%" name="type" id="type">
								<option value="image">Image</option>
								<option value="video">Video</option>
							</select>
						</div>

						<div class="col-sm-4">
							<label>Order Number</label>
							<input type="number" placeholder="Order" class="form-control" name="sort" id="sort" required="required">
						</div>

						<div class="col-sm-4">
							<label>Status</label>
							<select class="bootstrap-select" data-width="100%" name="status" id="status">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<label class="checkbox-inline">
								<input type="checkbox" id="status_link_new" name="status_link" value="1"> With Link
							</label>
						</div>
					</div>

					<div class="form-group with-link-new row">
						<div class="col-sm-8">
							<label>Link</label>
							<input type="text" class="form-control" name="link" id="link">
						</div>

						<div class="col-sm-4">
							<label>Type Link</label>
							<select class="bootstrap-select" data-width="100%" name="type_link" id="type_link">
								<option value="0">Self</option>
								<option value="1">Blank</option>
							</select>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-12">
							<label>Button on Slider</label>
						</div>

						<div class="col-sm-6">
							<label class="checkbox-inline">
								<input type="checkbox" name="download_googleplay" value="1"> Download Google Play
							</label>
						</div>

						<div class="col-sm-6">
							<label class="checkbox-inline">
								<input type="checkbox" name="download_appstore" value="1"> Download App Store
							</label>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Image</label>
							<input type="file" name="image" id="image" class="form-control" required="required">
							<span class="help-block">Image required if type video</span>
						</div>

						<div class="col-sm-6">
							<label>Video</label>
							<input type="file" name="video" id="video" class="form-control">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary submit-new-slider" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
    	if ($("#status_link_new").prop("checked") == false) {
    		$(".with-link-new").hide();
    	}
    });
    $("#status_link_new").click(function() {
    	if ($(this).prop("checked") == true) {
    		$(".with-link-new").show();
    	} else if ($(this).prop("checked") == false) {
    		$(".with-link-new").hide();
    	}
    });
</script>
<?php
if ($all_slider != null) {
    foreach ($all_slider as $value) { ?>
		<div id="update_slider_<?php echo $value->id_slider; ?>" class="modal fade hidden-reload">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Update Slider</h5>
					</div>

					<form action="#" method="POST" id="form-update-slider-<?php echo $value->id_slider; ?>" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="form-group row">
								<div class="col-sm-12"><div id="message-submit"></div></div>
								<div class="col-sm-6">
									<label>Text Top (English)</label>
									<input type="text" class="form-control" name="text_top" id="text_top" value="<?php echo $value->text_top; ?>">
								</div>
								<div class="col-sm-6">
									<label>Text Top (Indoensia)</label>
									<input type="text" class="form-control" name="text_top_id" id="text_top_id" value="<?php echo $value->text_top_id; ?>">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Text Bottom (English)</label>
									<input type="text" class="form-control" name="text_bottom" id="text_bottom" value="<?php echo $value->text_bottom; ?>">
								</div>
								<div class="col-sm-6">
									<label>Text Bottom (Indonesia)</label>
									<input type="text" class="form-control" name="text_bottom_id" id="text_bottom_id" value="<?php echo $value->text_bottom_id; ?>">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-4">
									<label>Type</label>
									<select class="bootstrap-select" data-width="100%" name="type" id="type">
										<option value="image" <?php echo ($value->type == "image" ? "selected" : ""); ?>>Image</option>
										<option value="video" <?php echo ($value->type == "video" ? "selected" : ""); ?>>Video</option>
									</select>
								</div>

								<div class="col-sm-4">
									<label>Order Number</label>
									<input type="number" placeholder="Order" class="form-control" name="sort" id="sort" required="required" value="<?php echo $value->sort; ?>">
								</div>

								<div class="col-sm-4">
									<label>Status</label>
									<select class="bootstrap-select" data-width="100%" name="status" id="status">
										<option value="1" <?php echo ($value->status == "1" ? "selected" : ""); ?>>Active</option>
										<option value="0" <?php echo ($value->status == "0" ? "selected" : ""); ?>>Inactive</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-12">
									<label class="checkbox-inline">
										<input type="checkbox" id="status_link_<?php echo $value->id_slider; ?>" name="status_link" value="1" <?php echo ($value->status_link == "1" ? "checked='checked'" : ""); ?>> With Link
									</label>
								</div>
							</div>

							<div class="form-group with-link-<?php echo $value->id_slider; ?> row">
								<div class="col-sm-8">
									<label>Link</label>
									<input type="text" class="form-control" name="link" id="link" value="<?php echo $value->link; ?>">
								</div>

								<div class="col-sm-4">
									<label>Type Link</label>
									<select class="bootstrap-select" data-width="100%" name="type_link" id="type_link">
										<option value="0" <?php echo ($value->type_link == "0" ? "selected" : ""); ?>>Self</option>
										<option value="1" <?php echo ($value->type_link == "1" ? "selected" : ""); ?>>Blank</option>
									</select>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-12">
									<label>Button on Slider</label>
								</div>

								<div class="col-sm-6">
									<label class="checkbox-inline">
										<input type="checkbox" name="download_googleplay" value="1" <?php echo ($value->download_googleplay == "1" ? "checked='checked'" : ""); ?>> Download Google Play
									</label>
								</div>

								<div class="col-sm-6">
									<label class="checkbox-inline">
										<input type="checkbox" name="download_appstore" value="1" <?php echo ($value->download_appstore == "1" ? "checked='checked'" : ""); ?>> Download App Store
									</label>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Image</label>
									<input type="file" name="image" id="image" class="form-control">
									<span class="help-block">Image required if type video</span>
								</div>

								<div class="col-sm-6">
									<label>Video</label>
									<input type="file" name="video" id="video" class="form-control">
									<span class="help-block">*)Please upload file mpeg | mkv | flv | mp4</span>
								</div>
							</div>
						</div>
						<input type="hidden" name="id_slider" value="<?php echo $value->id_slider; ?>">
						<input type="hidden" name="old_image" value="<?php echo $value->image; ?>">
						<input type="hidden" name="old_video" value="<?php echo $value->video; ?>">

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary submit-update-slider" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/* Submit form update slider */
		    $("#form-update-slider-<?php echo $value->id_slider; ?>").submit(function(e) {
		        $.ajax({
		            type: "POST",
		            url: "post_update_slider",
		            data: new FormData(this),
		            dataType: 'json',
		            processData: false,
		            contentType: false,
		            success: function(res) {
		                if (res.status == "success") {
		                    $("#form-update-slider-<?php echo $value->id_slider; ?> #message-submit").html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                    $("#form-update-slider-<?php echo $value->id_slider; ?>")[0].reset();
		                    setTimeout(function(){
		                    	$("#update_slider_<?php echo $value->id_slider; ?>").delay(3000).modal("hide");
		                    }, 3000);
		                } else {
		                    $("#form-update-slider-<?php echo $value->id_slider; ?> #message-submit").html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                }
		            }
		        });
		        e.preventDefault();
		    });

		    $(window).load(function() {
		    	if ($("#status_link_<?php echo $value->id_slider; ?>").prop("checked") == false) {
		    		$(".with-link-<?php echo $value->id_slider; ?>").hide();
		    	}
		    });
		    $("#status_link_<?php echo $value->id_slider; ?>").click(function() {
		    	if ($(this).prop("checked") == true) {
		    		$(".with-link-<?php echo $value->id_slider; ?>").show();
		    	} else if ($(this).prop("checked") == false) {
		    		$(".with-link-<?php echo $value->id_slider; ?>").hide();
		    	}
		    });
		</script>
    	<?php
    }
}
?>