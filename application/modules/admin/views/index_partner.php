<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Manage Partner</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_partner"><i class="icon-add position-left"></i> Add New</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <code>Partner</code> with <strong>Image</strong>
    </div>
    <table class="table datatable-partner">
        <thead>
            <tr>
                <th style="width: 150px;">Image</th>
                <th>Name</th>
                <th>Link</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($partner != null) {
                foreach ($partner as $value) { ?>
		            <tr>
		                <td>
		                	<div class="thumbnail" style="margin-bottom: 0px;border: none;">
								<img src="<?php echo base_url(); ?>assets/frontend/images/partner/<?php echo $value->image; ?>" />
							</div>
						</td>
		                <td><?php echo $value->name; ?></td>
		                <td><?php echo $value->link; ?></td>
		                <td><?php
		                	if ($value->status == 1) {
		                		echo '<span class="label label-success">Active</span>';
		                	} else {
		                		echo '<span class="label label-default">Inactive</span>';
		                	} ?>
		                </td>
		                <td class="text-center">
		                    <ul class="icons-list">
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="icon-menu9"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-toggle="modal" data-target="#update_partner_<?php echo $value->id_partner; ?>"><i class="icon-pencil7"></i> Update</a></li>
		                                <li><a href="./delete_partner/<?php echo $value->id_partner; ?>/<?php echo $value->image; ?>" onClick="return confirm('Are you sure want to delete?')"><i class="icon-trash"></i> Delete</a></li>
		                            </ul>
		                        </li>
		                    </ul>
		                </td>
		            </tr>
                	<?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id="new_partner" class="modal fade hidden-reload">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add New Partner</h5>
			</div>

			<form action="#" method="POST" id="form-new-partner" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<div id="message-submit"></div>
								<label>Name</label>
								<input type="text" class="form-control" name="name" id="name" required="required">
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-12">
								<label>Link</label>
								<input type="text" class="form-control" name="link" id="link" required="required">
								<span class="help-block">Link required with http://</span>
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-6">
								<label>Image</label>
								<input type="file" name="image" id="image" class="form-control" required="required">
								<span class="help-block">Image required to partner</span>
							</div>

							<div class="col-sm-6">
								<label>Status</label>
								<select class="bootstrap-select" data-width="100%" name="status" id="status">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								</select>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary submit-new-partner" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<?php
if ($partner != null) {
    foreach ($partner as $value) { ?>
		<div id="update_partner_<?php echo $value->id_partner; ?>" class="modal fade hidden-reload">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Update Partner</h5>
					</div>

					<form action="#" method="POST" id="form-update-partner-<?php echo $value->id_partner; ?>" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<div id="message-submit"></div>
										<label>Name</label>
										<input type="text" class="form-control" name="name" id="name" value="<?php echo $value->name; ?>" required="required">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-12">
										<label>Link</label>
										<input type="text" class="form-control" name="link" id="link" value="<?php echo $value->link; ?>" required="required">
										<span class="help-block">Link required with http://</span>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-6">
										<label>Image</label>
										<input type="file" name="image" id="image" class="form-control">
										<span class="help-block">Image required to partner</span>
									</div>

									<div class="col-sm-6">
										<label>Status</label>
										<select class="bootstrap-select" data-width="100%" name="status" id="status">
											<option value="1" <?php echo ($value->status == "1" ? "selected" : ""); ?>>Active</option>
											<option value="0" <?php echo ($value->status == "0" ? "selected" : ""); ?>>Inactive</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="id_partner" value="<?php echo $value->id_partner; ?>">
						<input type="hidden" name="old_image" value="<?php echo $value->image; ?>">

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary submit-update-partner" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/* Submit form update partner */
		    $("#form-update-partner-<?php echo $value->id_partner; ?>").submit(function(e) {
		        $.ajax({
		            type: "POST",
		            url: "post_update_partner",
		            data: new FormData(this),
		            dataType: 'json',
		            processData: false,
		            contentType: false,
		            success: function(res) {
		                if (res.status == "success") {
		                    $("#form-update-partner-<?php echo $value->id_partner; ?> #message-submit").html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                    $("#form-update-partner-<?php echo $value->id_partner; ?>")[0].reset();
		                    setTimeout(function(){
		                    	$("#update_partner_<?php echo $value->id_partner; ?>").delay(3000).modal("hide");
		                    }, 3000);
		                } else {
		                    $("#form-update-partner-<?php echo $value->id_partner; ?> #message-submit").html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                }
		            }
		        });
		        e.preventDefault();
		    });
		</script>
    	<?php
    }
}
?>