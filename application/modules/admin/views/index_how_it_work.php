<div class="row">
	<?php
	$status = ($this->session->flashdata('status') == "success") ? "success" : "danger";
	$message = $this->session->flashdata('message');
	if (isset($message)) { ?>
		<div class="col-md-12">
		    <div class="alert alert-<?php echo $status; ?> alert-styled-left alert-arrow-left alert-bordered">
		        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><?php echo $message; ?></div>
		</div>
		<?php
	}

	if ($how_it_work != null) {
	    foreach ($how_it_work as $value) { ?>
	    	<form action="post_update_hiw" method="POST" id="form-update-hiw" enctype="multipart/form-data">
				<div class="col-md-4">
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title"><?php echo $value->text; ?></h5>
							<div class="heading-elements">
								<ul class="icons-list">
									<button type="submit" class="btn btn-success btn-xs"><i class="icon-pencil7 position-left"></i> Update</button>
			                	</ul>
			            	</div>
						</div>

						<div class="panel-body">
							<div class="form-group">
								<div class="thumbnail col-md-6 col-md-offset-3" style="margin-bottom: 0px;border: none;">
									<img src="<?php echo base_url(); ?>assets/frontend/images/how_it_work/<?php echo $value->image; ?>" />
								</div>
								<input type="file" class="form-control" name="image">
							</div>

							<div class="form-group">
								<label>Title (English)</label>
								<input type="text" class="form-control" name="text" value="<?php echo $value->text; ?>" required="required">
							</div>

							<div class="form-group">
								<label>Description (English)</label>
								<textarea rows="5" cols="5" name="description" class="form-control" required="required"><?php echo $value->description; ?></textarea>
							</div>

							<div class="form-group">
								<label>Title (Indonesia)</label>
								<input type="text" class="form-control" name="text_id" value="<?php echo $value->text_id; ?>" required="required">
							</div>

							<div class="form-group">
								<label>Description (Indonesia)</label>
								<textarea rows="5" cols="5" name="description_id" class="form-control" required="required"><?php echo $value->description_id; ?></textarea>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="id_how_it_work" value="<?php echo $value->id_how_it_work; ?>">
				<input type="hidden" name="old_image" value="<?php echo $value->image; ?>">
			</form>
			<?php
	    }
	}
	?>
</div>