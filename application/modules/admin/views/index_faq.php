<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Manage FAQ</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_faq"><i class="icon-add position-left"></i> Add New</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <code>FAQ</code> with <strong>Sort Number</strong>
    </div>
    <table class="table datatable-faq">
        <thead>
            <tr>
                <th>Order</th>
                <th>Question</th>
                <th>Answer</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($all_faq != null) {
                foreach ($all_faq as $value) { ?>
		            <tr>
		                <td><?php echo $value->sort; ?></td>
		                <td><?php echo $value->tanya; ?></td>
		                <td><?php echo $value->jawab; ?></td>
		                <td class="text-center">
		                    <ul class="icons-list">
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="icon-menu9"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-toggle="modal" data-target="#update_faq_<?php echo $value->id_faq; ?>"><i class="icon-pencil7"></i> Update</a></li>
		                                <li><a href="./delete_faq/<?php echo $value->id_faq; ?>" onClick="return confirm('Are you sure want to delete?')"><i class="icon-trash"></i> Delete</a></li>
		                            </ul>
		                        </li>
		                    </ul>
		                </td>
		            </tr>
                	<?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id="new_faq" class="modal fade hidden-reload">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add New FAQ</h5>
			</div>

			<form action="#" method="POST" id="form-new-faq">
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-sm-12"><div id="message-submit"></div></div>
						<div class="col-sm-6">
							<label>Question (English)</label>
							<input type="text" class="form-control" name="tanya" id="tanya" required="required">
						</div>
						<div class="col-sm-6">
							<label>Question (Indonesia)</label>
							<input type="text" class="form-control" name="tanya_id" id="tanya_id" required="required">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Answer (English)</label>
							<textarea rows="10" cols="5" name="jawab" class="form-control" required="required"></textarea>
						</div>
						<div class="col-sm-6">
							<label>Answer (Indonesia)</label>
							<textarea rows="10" cols="5" name="jawab_id" class="form-control" required="required"></textarea>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-4">
							<label>Order Number</label>
							<input type="number" placeholder="Order" class="form-control" name="sort" id="sort" required="required">
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary submit-new-faq" value="Save">
				</div>
			</form>
		</div>
	</div>
</div>
<?php
if ($all_faq != null) {
    foreach ($all_faq as $value) { ?>
		<div id="update_faq_<?php echo $value->id_faq; ?>" class="modal fade hidden-reload">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Update FAQ</h5>
					</div>

					<form action="#" method="POST" id="form-update-faq-<?php echo $value->id_faq; ?>">
						<div class="modal-body">
							<div class="form-group row">
								<div class="col-sm-12"><div id="message-submit"></div></div>
								<div class="col-sm-6">
									<label>Question (English)</label>
									<input type="text" class="form-control" name="tanya" id="tanya" value="<?php echo $value->tanya; ?>" required="required">
								</div>
								<div class="col-sm-6">
									<label>Question (Indonesia)</label>
									<input type="text" class="form-control" name="tanya_id" id="tanya_id" value="<?php echo $value->tanya_id; ?>" required="required">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Answer (English)</label>
									<textarea rows="10" cols="5" name="jawab" class="form-control" style="margin-bottom: 20px;" required="required"><?php echo $value->jawab; ?></textarea>
								</div>
								<div class="col-sm-6">
									<label>Answer (Indonesia)</label>
									<textarea rows="10" cols="5" name="jawab_id" class="form-control" style="margin-bottom: 20px;" required="required"><?php echo $value->jawab_id; ?></textarea>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-4">
									<label>Order Number</label>
									<input type="number" placeholder="Order" class="form-control" name="sort" id="sort" required="required" value="<?php echo $value->sort; ?>">
								</div>
							</div>
						</div>
						<input type="hidden" name="id_faq" value="<?php echo $value->id_faq; ?>">

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary submit-update-faq" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/* Submit form update faq */
		    $("#form-update-faq-<?php echo $value->id_faq; ?>").submit(function(e) {
		        $.ajax({
		            type: "POST",
		            url: "post_update_faq",
		            data: new FormData(this),
		            dataType: 'json',
		            processData: false,
		            contentType: false,
		            success: function(res) {
		                if (res.status == "success") {
		                    $("#form-update-faq-<?php echo $value->id_faq; ?> #message-submit").html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                    setTimeout(function(){
		                    	$("#update_faq_<?php echo $value->id_faq; ?>").delay(3000).modal("hide");
		                    }, 3000);
		                } else {
		                    $("#form-update-faq-<?php echo $value->id_faq; ?> #message-submit").html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                }
		            }
		        });
		        e.preventDefault();
		    });
		</script>
    	<?php
    }
}
?>