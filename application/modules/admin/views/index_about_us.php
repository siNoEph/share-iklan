<div class="row">
	<?php
	$status = ($this->session->flashdata('status') == "success") ? "success" : "danger";
	$message = $this->session->flashdata('message');
	if (isset($message)) { ?>
		<div class="col-md-12">
		    <div class="alert alert-<?php echo $status; ?> alert-styled-left alert-arrow-left alert-bordered">
		        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button><?php echo $message; ?></div>
		</div>
		<?php
	}

	if ($about_us != null) { ?>
    	<form action="post_update_about_us" method="POST" id="form-update-about_us" enctype="multipart/form-data">
			<div class="col-md-12">
				<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title">Update About Us</h5>
					</div>

					<div class="panel-body" id="update_aboutus">
						<div class="form-group row">
							<div class="col-md-6">
								<div style="height: 300px;">
									<img src="<?php echo base_url(); ?>assets/frontend/images/about_us/<?php echo $about_us->image; ?>" style="width: 100%;" />
								</div>
							</div>
							<div class="col-md-6">
								<label>Image</label>
								<input type="file" name="image" id="image" class="form-control">
								<span class="help-block">*) Please upload file gif | jpg | png</span>
							</div>
							<div class="col-md-6" style="margin-top: 30px;">
								<label>Link FAQ : <b>faq</b></label><br>
								<label>Link Terms Conditions : <b>terms_conditions</b></label>
							</div>
						</div>

						<div class="form-group">
							<label>Text English</label>
							<textarea rows="5" cols="5" name="text" id="text" style="display: none;" class="form-control"><?php echo $about_us->text; ?></textarea>
							<div class="summernote-aboutus"><?php echo $about_us->text; ?></div>
						</div>

						<div class="form-group">
							<label>Text Indonesia</label>
							<textarea rows="5" cols="5" name="text_id" id="text_id" style="display: none;" class="form-control"><?php echo $about_us->text_id; ?></textarea>
							<div class="summernote-aboutus-id"><?php echo $about_us->text_id; ?></div>
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-success btn-xs pull-right"><i class="icon-pencil7 position-left"></i> Update</button>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="id_about_us" value="<?php echo $about_us->id_about_us; ?>">
			<input type="hidden" name="old_image" value="<?php echo $about_us->image; ?>">
		</form>
		<script type="text/javascript">
			$('.summernote-aboutus').summernote({
		        onChange: function() {
		            $("#update_aboutus #text").val($('.summernote-aboutus').code());
		        }
		    });
			$('.summernote-aboutus-id').summernote({
		        onChange: function() {
		            $("#update_aboutus #text_id").val($('.summernote-aboutus-id').code());
		        }
		    });
		</script>
		<?php
	}
	?>
</div>