<!-- Basic datatable -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Manage Article</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#new_article"><i class="icon-add position-left"></i> Add New</button>
                </li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <code>Article</code> with <strong>Image</strong>
    </div>
    <table class="table datatable-article">
        <thead>
            <tr>
                <th style="width: 150px;">Image</th>
                <th>Title</th>
                <th>Description</th>
                <th>Status</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
        	<?php
            if ($article != null) {
                foreach ($article as $value) { ?>
		            <tr>
		                <td>
		                	<div class="thumbnail" style="margin-bottom: 0px;">
								<img src="<?php echo base_url(); ?>assets/frontend/images/article/<?php echo $value->image; ?>" />
							</div>
						</td>
		                <td><?php echo $value->title; ?></td>
		                <td><?php echo substr($value->description, 0, 150)."..."; ?></td>
		                <td><?php
		                	if ($value->status == 1) {
		                		echo '<span class="label label-success">Active</span>';
		                	} else {
		                		echo '<span class="label label-default">Inactive</span>';
		                	} ?>
		                </td>
		                <td class="text-center">
		                    <ul class="icons-list">
		                        <li class="dropdown">
		                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                                <i class="icon-menu9"></i>
		                            </a>
		                            <ul class="dropdown-menu dropdown-menu-right">
		                                <li><a href="#" data-toggle="modal" data-target="#update_article_<?php echo $value->id_article; ?>"><i class="icon-pencil7"></i> Update</a></li>
		                                <li><a href="./delete_article/<?php echo $value->id_article; ?>/<?php echo $value->image; ?>" onClick="return confirm('Are you sure want to delete?')"><i class="icon-trash"></i> Delete</a></li>
		                            </ul>
		                        </li>
		                    </ul>
		                </td>
		            </tr>
                	<?php
                }
            }
            ?>
        </tbody>
    </table>
</div>
<div id="new_article" class="modal fade hidden-reload">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Add New Article</h5>
			</div>

			<form action="#" method="POST" id="form-new-article" enctype="multipart/form-data">
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-sm-12"><div id="message-submit"></div></div>
						<div class="col-sm-6">
							<label>Title (English)</label>
							<input type="text" class="form-control" name="title" id="title" required="required">
						</div>
						<div class="col-sm-6">
							<label>Title (Indonesia)</label>
							<input type="text" class="form-control" name="title_id" id="title_id" required="required">
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Description (English)</label>
							<textarea rows="5" cols="5" name="description" id="description" style="display: none;" class="form-control"></textarea>
							<div class="summernote"></div>
						</div>
						<div class="col-sm-6">
							<label>Description (Indonesia)</label>
							<textarea rows="5" cols="5" name="description_id" id="description_id" style="display: none;" class="form-control"></textarea>
							<div class="summernote_id"></div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
							<label>Image</label>
							<input type="file" name="image" id="image" class="form-control" required="required">
							<span class="help-block">Image required to article</span>
						</div>

						<div class="col-sm-6">
							<label>Status</label>
							<select class="bootstrap-select" data-width="100%" name="status" id="status">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<input type="submit" class="btn btn-primary submit-new-article" value="Save">
				</div>
			</form>			
			<script type="text/javascript">
				$('.summernote').summernote({
			        onChange: function() {
			            $("#form-new-article #description").val($('.summernote').code());
			        }
			    });
				$('.summernote_id').summernote({
			        onChange: function() {
			            $("#form-new-article #description_id").val($('.summernote_id').code());
			        }
			    });
			</script>
		</div>
	</div>
</div>
<?php
if ($article != null) {
    foreach ($article as $value) { ?>
		<div id="update_article_<?php echo $value->id_article; ?>" class="modal fade hidden-reload">
			<div class="modal-dialog modal-full">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h5 class="modal-title">Update article</h5>
					</div>

					<form action="#" method="POST" id="form-update-article-<?php echo $value->id_article; ?>" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="form-group row">
								<div class="col-sm-12"><div id="message-submit"></div></div>
								<div class="col-sm-6">
									<label>Title (English)</label>
									<input type="text" class="form-control" name="title" id="title" value="<?php echo $value->title; ?>" required="required">
								</div>
								<div class="col-sm-6">
									<label>Title (Indonesia)</label>
									<input type="text" class="form-control" name="title_id" id="title_id" value="<?php echo $value->title_id; ?>" required="required">
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Description (English)</label>
									<textarea rows="5" cols="5" name="description" id="description" style="display: none;" class="form-control"><?php echo $value->description; ?></textarea>
									<div class="summernote-<?php echo $value->id_article; ?>"><?php echo $value->description; ?></div>
								</div>
								<div class="col-sm-6">
									<label>Description (Indonesia)</label>
									<textarea rows="5" cols="5" name="description_id" id="description_id" style="display: none;" class="form-control"><?php echo $value->description_id; ?></textarea>
									<div class="summernote-id-<?php echo $value->id_article; ?>"><?php echo $value->description_id; ?></div>
								</div>
							</div>

							<div class="form-group row">
								<div class="col-sm-6">
									<label>Image</label>
									<input type="file" name="image" id="image" class="form-control">
									<span class="help-block">Image required to article</span>
								</div>

								<div class="col-sm-6">
									<label>Status</label>
									<select class="bootstrap-select" data-width="100%" name="status" id="status">
										<option value="1" <?php echo ($value->status == "1" ? "selected" : ""); ?>>Active</option>
										<option value="0" <?php echo ($value->status == "0" ? "selected" : ""); ?>>Inactive</option>
									</select>
								</div>
							</div>
						</div>
						<input type="hidden" name="id_article" value="<?php echo $value->id_article; ?>">
						<input type="hidden" name="old_image" value="<?php echo $value->image; ?>">

						<div class="modal-footer">
							<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							<input type="submit" class="btn btn-primary submit-update-article" value="Update">
						</div>
					</form>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			/* Submit form update article */
		    $("#form-update-article-<?php echo $value->id_article; ?>").submit(function(e) {
		        $.ajax({
		            type: "POST",
		            url: "post_update_article",
		            data: new FormData(this),
		            dataType: 'json',
		            processData: false,
		            contentType: false,
		            success: function(res) {
		                if (res.status == "success") {
		                    $("#form-update-article-<?php echo $value->id_article; ?> #message-submit").html('<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                    $("#form-update-article-<?php echo $value->id_article; ?>")[0].reset();
		                    setTimeout(function(){
		                    	$("#update_article_<?php echo $value->id_article; ?>").delay(3000).modal("hide");
		                    }, 3000);
		                } else {
		                    $("#form-update-article-<?php echo $value->id_article; ?> #message-submit").html('<div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered"><button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>' + res.message + '</div>');
		                }
		            }
		        });
		        e.preventDefault();
		    });

		    $('.summernote-<?php echo $value->id_article; ?>').summernote({
		        onChange: function() {
		            $("#update_article_<?php echo $value->id_article; ?> #description").val($('.summernote-<?php echo $value->id_article; ?>').code());
		        }
		    });
		    $('.summernote-id-<?php echo $value->id_article; ?>').summernote({
		        onChange: function() {
		            $("#update_article_<?php echo $value->id_article; ?> #description_id").val($('.summernote-id-<?php echo $value->id_article; ?>').code());
		        }
		    });
		</script>
    	<?php
    }
}
?>