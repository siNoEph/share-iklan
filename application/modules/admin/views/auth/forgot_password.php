<body class="login-cover">

  <!-- Page container -->
  <div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

          <!-- Form with validation -->
          <?php echo form_open("admin/auth/forgot_password");?>
			<div class="panel panel-body login-form">
				<div class="text-center">
					<div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
					<h5 class="content-group">Password recovery <small class="display-block">We'll send you instructions in email</small></h5>
				</div>

				<div class="form-group has-feedback">
					<?php echo form_input($identity);?>
					<div class="form-control-feedback">
						<i class="icon-mail5 text-muted"></i>
					</div>
				</div>

				<div class="form-group login-options">
					<label class="validation-error-label"><?php echo $message;?></label>
				</div>

				<button type="submit" class="btn bg-blue btn-block">Reset password <i class="icon-arrow-right14 position-right"></i></button>
			</div>
          <?php echo form_close();?>
          <!-- /form with validation -->


          <!-- Footer -->
          <div class="footer text-white">
            &copy; 2016. Adminpanel Share Iklan
          </div>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

</body>