<body class="login-cover">

  <!-- Page container -->
  <div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

      <!-- Main content -->
      <div class="content-wrapper">

        <!-- Content area -->
        <div class="content">

          <!-- Form with validation -->
          <?php echo form_open("./admin/auth/login", 'class="form-validate"');?>
            <div class="panel panel-body login-form">
              <div class="text-center">
                <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                <h5 class="content-group">Login to Adminpanel <small class="display-block">Your credentials</small></h5>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <?php echo form_input($identity);?>
                <div class="form-control-feedback">
                  <i class="icon-user text-muted"></i>
                </div>
              </div>

              <div class="form-group has-feedback has-feedback-left">
                <?php echo form_input($password);?>
                <div class="form-control-feedback">
                  <i class="icon-lock2 text-muted"></i>
                </div>
              </div>

              <div class="form-group login-options">
                <div class="row">
                  <div class="col-sm-6">
                    <label class="checkbox-inline">
                      <?php echo form_checkbox('remember', '1', FALSE, 'id="remember" class="styled" checked="checked"');?>
                      Remember
                    </label>
                  </div>

                  <div class="col-sm-6 text-right">
                    <a href="forgot_password">Forgot password?</a>
                  </div>
                </div>
                <label class="validation-error-label"><?php echo $message;?></label>
              </div>

              <div class="form-group">
                <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-circle-right2 position-right"></i></button>
              </div>
            </div>
          <?php echo form_close();?>
          <!-- /form with validation -->


          <!-- Footer -->
          <div class="footer text-white">
            &copy; 2016. Adminpanel Share Iklan
          </div>
          <!-- /footer -->

        </div>
        <!-- /content area -->

      </div>
      <!-- /main content -->

    </div>
    <!-- /page content -->

  </div>
  <!-- /page container -->

</body>