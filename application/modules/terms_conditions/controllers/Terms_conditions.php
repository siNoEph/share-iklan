<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_conditions extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('home/Home_model');
		$this->load->model('Terms_conditions_model');
		$this->load->helper('email');
		$this->load->library('email');
		$this->lang->load('bahasa','english');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index() {
		$data['term'] = $this->Terms_conditions_model->get_term();
		$data['all_article'] = $this->Home_model->get_all_article();
		$data['contact'] = $this->Home_model->get_contact();

		$this->load->view('index_terms_conditions', $data);
	}
}
