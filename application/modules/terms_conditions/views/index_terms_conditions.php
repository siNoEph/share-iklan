<?php $this->view('template/frontend/header_front'); ?>
<div class="light-wrapper">
    <div class="container padd-from-nav">
        <div class="blog row">
            <div class="col-sm-8 blog-content">
                <div class="blog-posts classic-view">
                    <div class="post">                        
                        <div class="box" style="padding: 30px;">
                            <h3 class="section-title text-uppercase">Terms Conditions</h3>
                            <div class="post-content">
                                <?php echo $this->session->userdata('site_lang') == 'english' ? $term->text : $term->text_id; ?>
                            </div>
                        </div>
                    </div>
                    <div class="divide30"></div>
                </div>
            </div>
            <!-- /.blog-content -->
            <?php $this->view('template/frontend/right_detail'); ?>
        </div>
    </div>
</div>
<?php $this->view('template/frontend/footer_front'); ?>