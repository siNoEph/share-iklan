<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Terms_conditions_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_term()
        {
                $query = $this->db->get('term');
                
                return $query->row();
        }
}