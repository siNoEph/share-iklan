<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_all_slider()
        {
                $this->db->where("status", 1);
                $this->db->order_by("sort", "asc"); 
                $query = $this->db->get('slider');
                
                return $query->result();
        }

        public function get_how_it_work()
        {
                $this->db->order_by("sort", "asc"); 
                $query = $this->db->get('how_it_work');
                
                return $query->result();
        }

        public function get_about_us()
        {
                $query = $this->db->get('about_us');
                
                return $query->row();
        }

        public function get_video_iklan()
        {
                $this->db->where("status", 1);
                $query = $this->db->get('video_iklan', 1);
                
                return $query->result();
        }

        public function get_all_testimony()
        {
                $this->db->where("status", 1);
                $query = $this->db->get('testimony', 5);
                
                return $query->result();
        }

        public function get_all_article()
        {
                $this->db->where("status", 1);
                $this->db->order_by("date", "desc"); 
                $query = $this->db->get('article', 5);
                
                return $query->result();
        }

        public function get_all_partner()
        {
                $this->db->where("status", 1);
                $query = $this->db->get('partner');
                
                return $query->result();
        }

        public function get_contact()
        {
                $query = $this->db->get('contact');
                
                return $query->row();
        }

        public function post_form_contact($data) {
                $this->db->insert('form_contact', $data);
                return true;
        }
}