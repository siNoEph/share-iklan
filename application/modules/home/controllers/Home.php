<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model');
		$this->load->helper('email');
		$this->load->library('email');
		$this->lang->load('bahasa','english');
		date_default_timezone_set('Asia/Jakarta');
	}

	public function index()
	{
		$data['all_slider'] = $this->Home_model->get_all_slider();
		$data['how_it_work'] = $this->Home_model->get_how_it_work();
		$data['about_us'] = $this->Home_model->get_about_us();
		$data['video_iklan'] = $this->Home_model->get_video_iklan();
		$data['all_testimony'] = $this->Home_model->get_all_testimony();
		$data['all_article'] = $this->Home_model->get_all_article();
		$data['all_partner'] = $this->Home_model->get_all_partner();
		$data['contact'] = $this->Home_model->get_contact();

		$this->load->view('index_home', $data);
	}

	public function submit_contact()
	{
		$now = new DateTime();
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$comment = $this->input->post('comment');
		$ouremail = $this->Home_model->get_contact()->email;

		$data = array(
			'name' => $name,
			'email' => $email,
			'comment' => $comment,
			'date_time' => $now->format('Y-m-d H:i:s')
		);

		if ($this->Home_model->post_form_contact($data) && $this->send_mail($ouremail, $email, $name, $comment)) {
			$this->response['status'] = "success";
			$this->response['message'] = "Terimakasih atas comment yang anda kirimkan.";
		} else {
			$this->response['status'] = "error";
			$this->response['message'] = "Maaf, data gagal di kirim.";
		}

		echo json_encode($this->response);
	}

	public function send_mail($ouremail, $email, $name, $comment)
	{
		// compose email
		$config = array (
			'mailtype' => 'html',
			'charset'  => 'utf-8',
			'priority' => '1'
		);
		$this->email->initialize($config);

		// compose email
		$this->email->from($email , $name);
		$this->email->to($ouremail);
		$this->email->subject("Comment or Feedback from Web");

		$dataemail['from'] = $email;
		$dataemail['name'] = $name;
		$dataemail['comment'] = $comment;
		$dataemail['ouremail'] = $this->Home_model->get_contact()->email;
		$dataemail['address'] = $this->Home_model->get_contact()->address;
		$dataemail['phone'] = $this->Home_model->get_contact()->phone;

		$this->email->message($this->load->view('email', $dataemail, true));

		$sendMail = $this->email->send();
		if($sendMail == TRUE) {
			return true;
		} else {
			return false;
		}
	}
}
