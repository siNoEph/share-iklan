<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/frontend/images/favicon.png">
    <title>Share Iklan</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/frontend/css/plugins.css" rel="stylesheet">
    <link href="assets/frontend/css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="assets/frontend/type/icons.css" rel="stylesheet">

    <script src="assets/frontend/js/jquery.min.js"></script>
</head>

<body class="onepage">
    <div id="preloader">
        <div class="textload">Loading...</div>
        <div id="status">
            <div class="spinner"></div>
        </div>
    </div>
    <main class="body-wrapper">
        <div id="top-header">
            <div class="row">
                <div class="col-md-6 left">
                    <div class="item-left"><?php echo $this->lang->line('welcome'); ?></div>
                    <div class="item-left"><?php echo $contact->email; ?></div>
                    <div class="item-left"><?php echo $contact->phone; ?></div>
                </div>
                <!-- .left -->
                <div class="col-md-6 right">
                    <select onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;" class="language">
                        <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                        <option value="indonesia" <?php if($this->session->userdata('site_lang') == 'indonesia') echo 'selected="selected"'; ?>>Indonesia</option>
                    </select>
                    <ul class="social">
                        <li><a href="<?php echo $contact->link_fb; ?>" target="_blank"><i class="icon-s-facebook"></i></a></li>
                        <li><a href="<?php echo $contact->link_ig; ?>" target="_blank"><i class="icon-s-instagram"></i></a></li>
                        <li><a href="<?php echo $contact->link_tw; ?>" target="_blank"><i class="icon-s-twitter"></i></a></li>
                    </ul>
                </div>
                <!-- .right -->
            </div>
        </div>
        </div>
        <div class="navbar" data-spy="affix" data-offset-top="44">
            <div class="navbar-header">
                <div class="basic-wrapper">
                    <div class="navbar-brand">
                        <a href="<?php echo base_url(); ?>"><img src="assets/frontend/images/logo.png" class="logo-light" alt="Share Iklan" /></a>
                    </div>
                    <a class="btn responsive-menu" data-toggle="collapse" data-target=".navbar-collapse"><i></i></a> </div>
            </div>
            <!-- /.navbar-header -->
            <nav class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="current"><a href="#home"><?php echo $this->lang->line('home'); ?></a></li>
                    <li><a href="#how-it-works"><?php echo $this->lang->line('how_it_works'); ?></a></li>
                    <li><a href="#about-us"><?php echo $this->lang->line('about_us'); ?></a></li>
                    <li><a href="#best-tvc"><?php echo $this->lang->line('best_tvc'); ?></a></li>
                    <li><a href="#testimony"><?php echo $this->lang->line('testimony'); ?></a></li>
                    <li><a href="#update"><?php echo $this->lang->line('update'); ?></a></li>
                    <li><a href="#partner"><?php echo $this->lang->line('partner'); ?></a></li>
                    <li><a href="#contact"><?php echo $this->lang->line('contact'); ?></a></li>
                    <!-- <li class="hidden-md hidden-lg"><a href="#">Login</a></li> -->
                </ul>
                <!-- /.navbar-nav -->
            </nav>
            <!-- /.navbar-collapse -->
            <div class="social-wrapper">
                <ul class="social naked navbar-right">
                    <li><a href="#"><?php echo $this->lang->line('login'); ?></a></li>
                </ul>
            </div>
        </div>
        <!-- /.navbar -->
        <section id="home">
            <div class="tp-fullscreen-container revolution">
                <div class="tp-fullscreen">
                    <ul>
                        <?php
                        if ($all_slider != null) {
                            $i = 1;
                            foreach ($all_slider as $value) {
                                if ($i%2==0) { $animate = "sft";$animate2 = "sfl";$animate3 = "sfr"; }
                                else { $animate = "sfr";$animate2 = "sfb";$animate3 = "sfl"; }
                                $target_link = $value->type_link == 0 ? "_self" : "_blank";

                                if ($value->type == "image") { ?>
                                    <li data-transition="fade">
                                        <img src="assets/frontend/images/slider/<?php echo $value->image; ?>" data-statuslink="<?php echo $value->status_link; ?>" data-link="<?php echo $value->link; ?>" data-targetlink="<?php echo $target_link; ?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" />
                                        <h1 class="tp-caption large <?php echo $animate; ?>" data-x="30" data-y="343" data-speed="900" data-start="800" data-easing="Sine.easeOut"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->text_top : $value->text_top_id; ?></h1>
                                        <div class="tp-caption medium <?php echo $animate2; ?>" data-x="30" data-y="428" data-speed="900" data-start="1500" data-easing="Sine.easeOut"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->text_bottom : $value->text_bottom_id; ?></div>

                                        <?php
                                        if ($value->download_googleplay == 1 || $value->download_appstore == 1) { ?>
                                            <div class="tp-caption medium <?php echo $animate3; ?>" data-x="30" data-y="528" data-speed="900" data-start="2000" data-easing="Sine.easeOut">
                                                <?php
                                                if ($value->download_googleplay == 1) { ?>
                                                    <a href="<?php echo $contact->googleplay; ?>" target="_blank">
                                                        <img src="assets/frontend/images/icon/googleplay.png" class="down-app">
                                                    </a>
                                                    <?php
                                                }
                                                if ($value->download_appstore == 1) { ?>
                                                    <a href="<?php echo $contact->appstore; ?>" target="_blank">
                                                        <img src="assets/frontend/images/icon/appstore.png" class="down-app">
                                                    </a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </li>
                                    <?php
                                }
                                elseif ($value->type == "video") { ?>
                                    <li data-transition="fade">
                                        <img src="assets/frontend/images/slider/<?php echo $value->image; ?>" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" />
                                        <h1 class="tp-caption large <?php echo $animate; ?>" data-x="30" data-y="343" data-speed="900" data-start="800" data-easing="Sine.easeOut" style="z-index: 2;"><?php echo $value->text_top; ?></h1>
                                        <div class="tp-caption medium <?php echo $animate2; ?>" data-x="30" data-y="428" data-speed="900" data-start="1500" data-easing="Sine.easeOut" style="z-index: 2;"><?php echo $value->text_bottom; ?></div>
                                        <div class="tp-caption tp-fade fadeout fullscreenvideo" data-x="0" data-y="0" data-speed="1000" data-start="1100" data-easing="Power4.easeOut" data-elementdelay="0.01" data-endelementdelay="0.1" data-endspeed="1500" data-endeasing="Power4.easeIn" data-autoplay="true" data-autoplayonlyfirsttime="false" data-nextslideatend="true" data-dottedoverlay="twoxtwo" data-volume="mute" data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on" style="z-index: 1;">
                                            <?php echo $value->status_link == 1 ? '<a href="'.$value->link.'" target="'.$target_link.'">' : '' ?>
                                                <video class="" preload="none" width="100%" height="100%">
                                                    <source src="assets/frontend/video/<?php echo $value->video; ?>" type="video/mp4" />
                                                </video>
                                            <?php echo $value->status_link == 1 ? '</a>' : '' ?>
                                        </div>
                                        <?php
                                        if ($value->download_googleplay == 1 || $value->download_appstore == 1) { ?>
                                            <div class="tp-caption medium <?php echo $animate3; ?>" data-x="30" data-y="528" data-speed="900" data-start="2000" data-easing="Sine.easeOut">
                                                <?php
                                                if ($value->download_googleplay == 1) { ?>
                                                    <a href="<?php echo $contact->googleplay; ?>" target="_blank">
                                                        <img src="assets/frontend/images/icon/googleplay.png" class="down-app">
                                                    </a>
                                                    <?php
                                                }
                                                if ($value->download_appstore == 1) { ?>
                                                    <a href="<?php echo $contact->appstore; ?>" target="_blank">
                                                        <img src="assets/frontend/images/icon/appstore.png" class="down-app">
                                                    </a>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </li>
                                    <?php
                                }
                                $i++;
                            }
                        }
                        ?>
                    </ul>
                    <div class="tp-bannertimer tp-bottom"></div>
                </div>
            </div>
        </section>
        <!--/#home -->
        <section id="how-it-works">
            <div class="light-wrapper">
                <div class="container inner3">
                    <div class="thin">
                        <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('how_it_works'); ?></h3>
                    </div>
                    <div class="divide10"></div>
                    <div class="row facts">
                        <?php
                        if ($how_it_work != null) {
                            foreach ($how_it_work as $value) { ?>
                                <div class="col-sm-4 col-xs-4 text-center">
                                    <div class="icon-large"><img src="assets/frontend/images/how_it_work/<?php echo $value->image; ?>" id="hiw-<?php echo $value->id_how_it_work; ?>"></div>
                                    <p class="facts-text" id="hiw-<?php echo $value->id_how_it_work; ?>">
                                        <?php echo $this->session->userdata('site_lang') == 'english' ? $value->text : $value->text_id; ?>
                                    </p>
                                    <div class="arrow-up arrow-<?php echo $value->id_how_it_work; ?>"></div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="clearfix"></div>
                            <?php
                            foreach ($how_it_work as $value) { ?>
                                <div class="col-sm-12 detail-<?php echo $value->id_how_it_work; ?>">
                                    <p><?php echo $this->session->userdata('site_lang') == 'english' ? $value->description : $value->description_id; ?></p>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <!--/#how-it-works -->
        <div class="parallax parallax4 inverse-wrapper customers">
            <div class="container inner3">
                <div class="thin">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('join_us'); ?></h3>
                    <p class="text-center">&nbsp</p>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <input type="submit" class="btn col-sm-12 login-fb" value="Sign in with Facebook">
                    </div>
                    <div class="col-sm-8">
                        <form action="" method="post" class="vanilla vanilla-form parallax-form" novalidate="novalidate">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-field">
                                        <label>
                                            <input type="text" name="username" placeholder="Username" required="required">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-field">
                                        <label>
                                            <input type="email" name="email" placeholder="E-mail" required="required">
                                        </label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-sm-4">
                                    <div class="form-field">
                                        <label>
                                            <input type="password" name="password" placeholder="Password" required="required">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-field">
                                        <label>
                                            <input type="password" name="confirm_password" placeholder="Confirm Password" required="required">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-field">
                                        <label>
                                            <input type="submit" class="btn btn-red col-sm-12" value="SIGN UP" data-error="Fix errors" data-processing="Sending..." data-success="Thank you!">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <footer class="notification-box"></footer>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.parallax -->
        <section id="about-us">
            <div class="dark-wrapper">
                <div class="col-image">
                    <div class="bg-wrapper col-md-6 col-md-offset-6">
                        <div class="bg-holder" style="background-image: url(assets/frontend/images/about_us/<?php echo $about_us->image; ?>);"></div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 inner4">
                                <h3 class="section-title text-uppercase"><?php echo $this->lang->line('about_us'); ?></h3>
                                <?php echo $this->session->userdata('site_lang') == 'english' ? $about_us->text : $about_us->text_id; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#about-us -->
        <?php
        if ($video_iklan != null) {
            foreach ($video_iklan as $value) {
                $target_link = $value->type_link == 0 ? "_self" : "_blank"; ?>
                <div class="outer-wrap inverse-wrapper">
                    <?php echo $value->status_link == 1 ? '<a href="'.$value->link.'" target="'.$target_link.'">' : '' ?>
                        <div id="video-wrap" class="video-wrap">
                            <video preload="metadata" autoplay loop id="video-office">
                                <source src="assets/frontend/video/<?php echo $value->video; ?>" type="video/mp4">
                            </video>
                            <div class="content-overlay container">
                                <div class="headline text-center">
                                    <h3 class="section-title">
                                        <?php echo $this->session->userdata('site_lang') == 'english' ? $value->title : $value->title_id; ?>
                                    </h3>
                                    <p style="color: #cdcdcd;">
                                        <?php echo $this->session->userdata('site_lang') == 'english' ? $value->description : $value->description_id; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    <?php echo $value->status_link == 1 ? '</a>' : '' ?>
                </div>
                <?php
            }
        }
        ?>
        <section id="best-tvc">
            <div class="light-wrapper">
                <div class="container inner3">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('best_tvc'); ?></h3>
                    <div class="divide20"></div>
                    <div class="row grid-view bp30">
                        <div class="col-sm-3 text-center">
                            <a class="fancybox-video hover-overlay" data-rel="portfolio" href="assets/frontend/video/fish.mp4" data-desc-id="desc-1">
                                <figure><span class="overlay-block"></span><img src="assets/frontend/images/art/t1.jpg" alt="" /></figure>
                            </a>
                            <div id="desc-1">
                                <h4 class="post-title">Nescafe : Gadis Cantik</h4>
                                <div class="meta">Matari Ad</div>
                                <p>Duration 00:31 | Year 2016 | Share 7.600</p>
                                <ul class="social naked bigger text-center">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-instagram"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="fancybox-video hover-overlay" data-rel="portfolio" href="https://www.youtube.com/watch?v=7mjrIWHkm2w" data-desc-id="desc-2">
                                <figure><span class="overlay-block"></span><img src="assets/frontend/images/art/t2.jpg" alt="" /></figure>
                            </a>
                            <div id="desc-2">
                                <h4 class="post-title">Honda : Vario (Agnes)</h4>
                                <div class="meta">Matari Ad</div>
                                <p>Duration 00:31 | Year 2016 | Share 7.600</p>
                                <ul class="social naked bigger text-center">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-instagram"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="fancybox-video hover-overlay" data-rel="portfolio" href="assets/frontend/video/fish.mp4" data-desc-id="desc-3">
                                <figure><span class="overlay-block"></span><img src="assets/frontend/images/art/t3.jpg" alt="" /></figure>
                            </a>
                            <div id="desc-3">
                                <h4 class="post-title">LINE : Perpus (Maudy)</h4>
                                <div class="meta">Matari Ad</div>
                                <p>Duration 00:31 | Year 2016 | Share 7.600</p>
                                <ul class="social naked bigger text-center">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-instagram"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 text-center">
                            <a class="fancybox-video hover-overlay" data-rel="portfolio" href="assets/frontend/video/fish.mp4" data-desc-id="desc-4">
                                <figure><span class="overlay-block"></span><img src="assets/frontend/images/art/t4.jpg" alt="" /></figure>
                            </a>
                            <div id="desc-4">
                                <h4 class="post-title">LOREAL : Total (Dian S)</h4>
                                <div class="meta">Matari Ad</div>
                                <p>Duration 00:31 | Year 2016 | Share 7.600</p>
                                <ul class="social naked bigger text-center">
                                    <li><a href="#"><i class="icon-s-facebook"></i></a></li>
                                    <li><a href="#"><i class="icon-s-instagram"></i></a></li>
                                    <li><a href="#"><i class="icon-s-twitter"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#best-tvc -->
        <section id="testimony">
            <div class="dark-wrapper">
                <div class="container tp50">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('testimony'); ?></h3>
                    <div class="divide30"></div>
                    <div class="row">
                        <div class="testimony-slider">
                            <?php
                            if ($all_testimony != null) {
                                foreach ($all_testimony as $value) { ?>
                                    <div class="item">
                                        <div class="col-md-7 col-xs-12">
                                            <h2><?php echo $value->name; ?></h2>
                                            <blockquote>
                                                <p><?php echo $this->session->userdata('site_lang') == 'english' ? $value->testimony : $value->testimony_id; ?></p>
                                            </blockquote>
                                        </div>
                                        <div class="col-md-4 col-xs-12 pull-right img-testimony">
                                            <figure class="bm0"><img src="assets/frontend/images/testimony/<?php echo $value->image; ?>" alt="<?php echo $value->name; ?>"></figure>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#testimony -->
        <section id="update">
            <div class="light-wrapper">
                <div class="container inner4">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('update'); ?></h3>
                    <div class="divide10"></div>
                    <div class="carousel-wrapper">
                        <div class="carousel carousel-boxed blog">
                            <?php
                            if ($all_article != null) {
                                foreach ($all_article as $value) { ?>
                                    <div class="item post">                                    
                                        <a href="<?php echo base_url()."article/detail/".$value->slug; ?>">
                                            <figure class="main"><img src="assets/frontend/images/article/<?php echo $value->image; ?>" /></figure>
                                        </a>
                                        <div class="box text-center">
                                            <h4 class="post-title"><a href="<?php echo base_url()."article/detail/".$value->slug; ?>"><?php echo $this->session->userdata('site_lang') == 'english' ? $value->title : $value->title_id; ?></a></h4>
                                            <div class="meta"><span class="date"><i class="icon-calendar-1"></i> <?php echo date("d M Y", strtotime($value->date)); ?></span></div>
                                            <?php echo $this->session->userdata('site_lang') == 'english' ? substr($value->description, 0, 140)."..." : substr($value->description_id, 0, 140)."..."; ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="<?php echo base_url(); ?>article">
                            <button class="btn btn-blue"><?php echo $this->lang->line('see_all'); ?></button>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--/#update -->
        <section id="partner">
            <div class="dark-wrapper">
                <div class="container inner2">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('partner'); ?></h3>
                    <div class="divide20"></div>
                    <div class="carousel-wrapper">
                        <div class="carousel clients">
                            <?php
                            if ($all_partner != null) {
                                foreach ($all_partner as $value) { ?>
                                    <div class="item">
                                        <a href="<?php echo $value->link; ?>" target="_blank"><img src="assets/frontend/images/partner/<?php echo $value->image; ?>" alt="<?php echo $value->name; ?>" /></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#partner -->
        <section id="contact">
            <div class="red-wrapper">
                <div class="container inner3">
                    <h3 class="section-title text-center text-uppercase"><?php echo $this->lang->line('contact'); ?></h3>
                    <div class="divide10"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div id="map"></div>
                            <script>
                            function initMap() {
                                var mapDiv = document.getElementById('map');
                                var latLng = {
                                    lat: <?php echo $contact->latitude; ?>,
                                    lng: <?php echo $contact->longitude; ?>
                                };
                                var map = new google.maps.Map(mapDiv, {
                                    center: latLng,
                                    zoom: 15,
                                    zoomControl: true,
                                    zoomControlOptions: {
                                        style: google.maps.ZoomControlStyle.DEFAULT,
                                    },
                                    disableDoubleClickZoom: false,
                                    mapTypeControl: false,
                                    mapTypeControlOptions: {
                                        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                                    },
                                    scaleControl: true,
                                    scrollwheel: false,
                                    streetViewControl: true,
                                    draggable: true,
                                    overviewMapControl: false,
                                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                                });
                                var contentString = '<div id="content"><h2 id="firstHeading" class="firstHeading"><?php echo $contact->name; ?></h2><div id="bodyContent"><p><i class="icon-location"></i> <?php echo $contact->address; ?><br /><i class="icon-phone"></i> <?php echo $contact->phone; ?> &nbsp <i class="icon-mail"></i> <?php echo $contact->email; ?></p></div></div>';

                                var infowindow = new google.maps.InfoWindow({
                                    content: contentString
                                });
                                var marker = new google.maps.Marker({
                                    position: latLng,
                                    map: map,
                                    title: '<?php echo $contact->name; ?>'
                                });
                                marker.addListener('click', function() {
                                    infowindow.open(map, marker);
                                });
                            }
                            </script>
                            <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiUu-1jHySHuJkcPy_pucOoqy7Dcs7ABU&callback=initMap"></script>
                        </div>
                        <div class="col-sm-6">
                            <h3 class="text-center"><?php echo $contact->name; ?></h3>
                            <ul class="contact-info text-center">
                                <li><i class="icon-location"></i><?php echo $contact->address; ?></li>
                                <li><i class="icon-phone"></i><a href="tel:<?php echo $contact->phone; ?>"><?php echo $contact->phone; ?></a></li>
                                <li><i class="icon-mail"></i><a href="mailto:<?php echo $contact->email; ?>"><?php echo $contact->email; ?></a> </li>
                            </ul>
                            <div class="divide30"></div>
                            <div class="form-container">
                                <form action="#" method="POST" class="vanilla vanilla-form form-contact">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="text" name="name" id="name-contact" placeholder="<?php echo $this->lang->line('your_name'); ?>" required>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-field">
                                                <label>
                                                    <input type="email" name="email" id="email-contact" placeholder="<?php echo $this->lang->line('your_email'); ?>" required>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea name="message" id="comment-contact" placeholder="<?php echo $this->lang->line('your_comment'); ?>" required></textarea>
                                    <input type="submit" class="btn btn-red col-sm-12 submit-contact" value="Send">
                                    <footer class="notification-box"></footer>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/#contact -->
        <footer class="footer inverse-wrapper text-center">
            <div class="container inner">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget">
                            <ul class="social">
                                <li><a href="<?php echo $contact->link_fb; ?>" target="_blank"><i class="icon-s-facebook"></i></a></li>
                                <li><a href="<?php echo $contact->link_ig; ?>" target="_blank"><i class="icon-s-instagram"></i></a></li>
                                <li><a href="<?php echo $contact->link_tw; ?>" target="_blank"><i class="icon-s-twitter"></i></a></li>
                            </ul>
                        </div>
                        <div class="widget">
                            <ul class="tag-list">
                                <li><a href="#home" class="btn"><?php echo $this->lang->line('home'); ?></a></li>
                                <li><a href="#how-it-works" class="btn"><?php echo $this->lang->line('how_it_works'); ?></a></li>
                                <li><a href="#about-us" class="btn"><?php echo $this->lang->line('about_us'); ?></a></li>
                                <li><a href="#best-tvc" class="btn"><?php echo $this->lang->line('best_tvc'); ?></a></li>
                                <li><a href="#testimony" class="btn"><?php echo $this->lang->line('testimony'); ?></a></li>
                                <li><a href="#update" class="btn"><?php echo $this->lang->line('update'); ?></a></li>
                                <li><a href="#partner" class="btn"><?php echo $this->lang->line('partner'); ?></a></li>
                                <li><a href="#contact" class="btn"><?php echo $this->lang->line('contact'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 tm20">
                        <p>Copyright © 2016 ShareIklan. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- /footer -->
    </main>
    <div class="modal fade" id="popupMessage" tabindex="-1" role="dialog" aria-labelledby="Message">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                </div>
                <div class="modal-body">
                    <p id="textMessage"></p>
                </div>
            </div>
        </div>
    </div>

    <!--/.body-wrapper -->
    <script src="assets/frontend/js/bootstrap.min.js"></script>
    <script src="assets/frontend/js/plugins.js"></script>
    <script src="assets/frontend/js/classie.js"></script>
    <script src="assets/frontend/js/jquery.themepunch.tools.min.js"></script>
    <script src="assets/frontend/js/scripts.js"></script>
</body>

</html>
