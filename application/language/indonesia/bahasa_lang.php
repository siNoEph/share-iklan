<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Top Menu
$lang['welcome'] = 'Selamat datang di web kami';
$lang['home'] = 'Beranda';
$lang['how_it_works'] = 'Cara Kerja';
$lang['about_us'] = 'Tentang Kami';
$lang['best_tvc'] = 'TVC Terbaik';
$lang['testimony'] = 'Testimoni';
$lang['update'] = 'Terbaru';
$lang['partner'] = 'Mitra';
$lang['contact'] = 'Kontak';
$lang['login'] = 'MASUK';

// Body Web
$lang['join_us'] = 'Bergabung dengan Kami';
$lang['see_all'] = 'Lihat Semua';
$lang['your_name'] = 'Nama Anda';
$lang['your_email'] = 'E-mail Anda';
$lang['your_comment'] = 'Komentar atau Tanggapan Anda';

// Detail Article
$lang['search'] = 'Pencarian';
$lang['text_search'] = 'Mencari sesuatu';
$lang['button_search'] = 'Cari';
$lang['other_post'] = 'Lainnya';