<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Top Menu
$lang['welcome'] = 'Welcome to Our Site';
$lang['home'] = 'Home';
$lang['how_it_works'] = 'How It Works';
$lang['about_us'] = 'About Us';
$lang['best_tvc'] = 'Best TVC';
$lang['testimony'] = 'Testimony';
$lang['update'] = 'Update';
$lang['partner'] = 'Partner';
$lang['contact'] = 'Contact';
$lang['login'] = 'LOGIN';

// Body Web
$lang['join_us'] = 'Join Us';
$lang['see_all'] = 'See All';
$lang['your_name'] = 'Your Name';
$lang['your_email'] = 'Your E-mail';
$lang['your_comment'] = 'Your Comment or Feedback';

// Detail Article
$lang['search'] = 'Search';
$lang['text_search'] = 'Search something';
$lang['button_search'] = 'Find';
$lang['other_post'] = 'Other Posts';